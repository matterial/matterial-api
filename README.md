# matterial-api

This project provides the Api objects for using the Matterial application. 

The class `StatusCode` contains all defined error codes of the application.  
They are contained in the header with the names `mtrStatusCode`.  
The header `mtrStatusMsg` usually contains a (English) description of the error. 

Additionally, the simple implementation of an API client and some examples are included. 

Please see all API documentation on [our website](https://www.matterial.com/de/documentation/api/).

[![](https://jitpack.io/v/com.gitlab.matterial/matterial-api.svg)](https://jitpack.io/#com.gitlab.matterial/matterial-api)
