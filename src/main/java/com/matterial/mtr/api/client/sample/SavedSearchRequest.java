package com.matterial.mtr.api.client.sample;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.matterial.mtr.api.client.MtrRestClient;
import com.matterial.mtr.api.object.AdditionalProperty;
import com.matterial.mtr.api.object.Attachment;
import com.matterial.mtr.api.object.Document;
import com.matterial.mtr.api.object.ListResult;
import com.matterial.mtr.api.object.LoginData;

/**
 * Sample client `SavedSearchRequest` <br />
 * * login (simple) <br />
 * * gets some documents by SavedSearchId <br />
 * * get the main file content <br />
 * * Prints all related Additional Properties <br />
 * * Prints the attachments <br />
 * * Prints the content  <br />
 */
public class SavedSearchRequest {

    /**
     * sample-client
     */
    public static void main(String[] args) {
        String matterialPath = MtrRestClient.PATH_LIVE;
        String userName = "user";
        String passwd = "***";
        long savedSearchId = 0L;
        if(args != null) {
            int i=0;
            for (String arg : args) {
                if(i==0) {
                    matterialPath = arg;
                }
                else if(i==1) {
                    userName = arg;
                }
                else if(i==2) {
                    passwd = arg;
                }
                else if(i==3) {
                    savedSearchId = Long.parseLong(arg);
                }
                i++;
            }
        }
        System.out.println("Sample Matterial Client: SavedSearchRequest");
        System.out.println("Using matterialPath: "+matterialPath);
        System.out.println("Using userName: "+userName);

        // *** init the client with matterialPath;
        MtrRestClient client = new MtrRestClient(matterialPath);
        // *** try to login, this returns some session data (LoginData);
        LoginData ld = client.simpleLogin(userName, passwd);
        // *** output the display name of current instance;
        System.out.println("CURRENT DATA SOURCE: " + ld.getCurrentDataSource().getDisplayName());

        // *** load some documents by a saved search id;
        ListResult<Document> lr = client.loadDocumentsBySavedSearch(savedSearchId, // savedSearchId
                                                                    false,  // loadRoleRights
                                                                    false,  // loadCategoriesPublicOnly
                                                                    false,  // loadCategories
                                                                    false,  // loadResponsibles
                                                                    false,  // loadAuthors
                                                                    false,  // loadLastAuthorOnly
                                                                    false,  // loadFollowers
                                                                    false,  // loadAmIFollowing
                                                                    false,  // loadMarkedAsHelpfulBy
                                                                    true,   // loadAttachments
                                                                    true,   // loadAdditionalProperties
                                                                    false,  // loadComments
                                                                    10      // limit
                                                                    );
        List<Document> docs = lr.getResults();
        long count = lr.getTotalHits();
        System.out.println("COUNT OF DEFAULT DOCUMENTS: " + count);
        // *** print some data for each document;
        for (Document document : docs) {
            System.out.println("DEFAULT DOCUMENT FOUND: "+document.getId()+"/"+document.getLanguageVersionLanguageKey()+" => "+document.getLanguageVersionTitle());
            // ** for each additional property;
            for (AdditionalProperty ap : document.getAdditionalProperties()) {
                System.out.println("ADDITIONAL PROPERTY: id: "+ap.getId()+", type: "+ap.getPropertyType()+", name: "+ap.getName());
            }
            // *** for each attachment;
            for (Attachment att : document.getAttachments()) {
                System.out.println("ATTACHMENT: "+att.getId()+" => "+att.getName());
                // *** load file content of the attachment;
                try(InputStream attStream = client.loadAttachmentContent(document.getId(), document.getLanguageVersionId(), att.getId())) {
                    System.out.println("ATTACHMENT: "+attStream);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // *** load the main file of the document;
            String content = client.loadDocumentContent(document.getId(), document.getLanguageVersionLanguageKey());
            System.out.println("DEFAULT DOCUMENT CONTENT: \n" + content+"\n*********************************************************\n\n");
        }
        // *** logout;
        client.logout();
    }

}
