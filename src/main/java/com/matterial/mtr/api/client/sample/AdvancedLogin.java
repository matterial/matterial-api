package com.matterial.mtr.api.client.sample;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.matterial.mtr.api.client.MtrRestClient;
import com.matterial.mtr.api.object.AdditionalProperty;
import com.matterial.mtr.api.object.Attachment;
import com.matterial.mtr.api.object.Document;
import com.matterial.mtr.api.object.ListResult;
import com.matterial.mtr.api.object.LoginData;

/**
 * Sample client `AdvancedLogin` <br />
 * * login (advanced) <br />
 * * gets some documents <br />
 * * get the main file content <br />
 * * Prints all related Additional Properties <br />
 * * Prints the attachments <br />
 * * Prints the content  <br />
 */
public class AdvancedLogin {

    /**
     * sample-usage;
     */
    public static void main(String[] args) {
        String matterialPath = MtrRestClient.PATH_LIVE;
        String userName = "user";
        String passwd = "***";
        if(args != null) {
            int i=0;
            for (String arg : args) {
                if(i==0) {
                    matterialPath = arg;
                }
                else if(i==1) {
                    userName = arg;
                }
                else if(i==2) {
                    passwd = arg;
                }
                i++;
            }
        }
        System.out.println("Sample Matterial Client: AdvancedLogin");
        System.out.println("Using matterialPath: "+matterialPath);
        System.out.println("Using userName: "+userName);

        MtrRestClient client = new MtrRestClient(matterialPath);
        LoginData ld = client.advancedLogin(userName, passwd);
        System.out.println("CURRENT DATA_SOURCE: " + ld.getCurrentDataSource().getDisplayName());

        // *** load 10 documents in all languages;
        ListResult<Document> lr = client.loadDocuments(null,   // languageKey
                                                       false,  // loadRoleRights
                                                       false,  // loadCategoriesPublicOnly
                                                       false,  // loadCategories
                                                       false,  // loadResponsibles
                                                       false,  // loadAuthors
                                                       false,  // loadLastAuthorOnly
                                                       false,  // loadFollowers
                                                       false,  // loadAmIFollowing
                                                       false,  // loadMarkedAsHelpfulBy
                                                       true,   // loadAttachments
                                                       true,   // loadAdditionalProperties
                                                       false,  // loadComments
                                                       "createTimeInSeconds", // orderBy
                                                       "asc",  // orderDir
                                                       10      // limit
                                                       );
        List<Document> docs = lr.getResults();
        long count = lr.getTotalHits();
        System.out.println("COUNT OF DOCUMENTS: " + count);
        for (Document document : docs) {
            System.out.println("DOCUMENT FOUND: "+document.getId()+"/"+document.getLanguageVersionLanguageKey()+" => "+document.getLanguageVersionTitle());
            for (AdditionalProperty ap : document.getAdditionalProperties()) {
                System.out.println("ADDITIONAL PROPERTY: id: "+ap.getId()+", type: "+ap.getPropertyType()+", name: "+ap.getName());
            }
            List<Attachment> atts = document.getAttachments();
            for (Attachment att : atts) {
                System.out.println("ATTACHMENT: "+att.getId()+" => "+att.getName());
                try(InputStream attStream = client.loadAttachmentContent(document.getId(), document.getLanguageVersionId(), att.getId())) {
                    System.out.println("ATTACHMENT: "+attStream);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            String content = client.loadDocumentContent(document.getId(), document.getLanguageVersionLanguageKey());
            System.out.println("DOCUMENT CONTENT: \n" + content+"\n*********************************************************\n\n");
        }
    }

}
