package com.matterial.mtr.api.client;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.CharEncoding;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.matterial.mtr.api.Api;
import com.matterial.mtr.api.object.AdditionalProperty;
import com.matterial.mtr.api.object.Category;
import com.matterial.mtr.api.object.CategoryType;
import com.matterial.mtr.api.object.CompanyInfoItem;
import com.matterial.mtr.api.object.DataSource;
import com.matterial.mtr.api.object.Document;
import com.matterial.mtr.api.object.ListResult;
import com.matterial.mtr.api.object.LoginData;
import com.matterial.mtr.api.object.Logon;
import com.matterial.mtr.api.object.Person;
import com.matterial.mtr.api.object.Role;
import com.matterial.mtr.api.object.TempFileDescriptor;

/**
 * <strong>MtrRestClient</strong>: Simple client that allows to access `matterial-backend` via REST.
 */
public class MtrRestClient extends RestClient {

    private static final Logger logger = LoggerFactory.getLogger(MtrRestClient.class);

    public static final String PATH_LIVE = "https://my.matterial.com/";
    public static final String PATH_LOCALHOST = "http://localhost:8080/";

    protected String basePath;

    /**
     * The default constructor inits the `basePath` to the application's backend.
     *
     * @param path something like: `https://my.matterial.com/`
     */
    public MtrRestClient(String path) {
        this.updateBasePath(path);
    }

    /**
     * (re)inits the `basePath` to the application's backend.
     *
     * @param path something like: `https://my.matterial.com/`
     */
    public void updateBasePath(String path) {
        String newPath = null;
        if(path != null && path.startsWith("http")) {
            try {
                URI uri = new URI(path);
                newPath = uri.getScheme()+"://"+uri.getHost()+(uri.getPort()>0?":"+uri.getPort():"")+"/mtr-backend/";
            }
            catch (URISyntaxException e) {
                logger.warn("updateBasePath() - ", e);
            }
        }
        if(newPath != null && !newPath.trim().isEmpty()) {
            this.basePath = newPath.trim();
        }
        logger.trace("updateBasePath() - basePath: {}", this.basePath);
    }

    /**
     * Simple login with user/password. This does not support changing servers.
     *
     * Used in live Matterial-App.
     */
    public LoginData simpleLogin(String userName, String password) {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.LOGON;
        logger.trace("login()");
        LoginData loginData = null;
        Logon logon = new Logon(userName, password);
        try(Response resp = this.post(path, logon);) {
            this.updateSession(resp);
            loginData = resp.readEntity(LoginData.class);
            logger.trace("login() - sessionId: {}", this.sessionId);
        }
        return loginData;
    }

    /**
     * Simple logout.
     *
     * Used in live Matterial-App.
     */
    public void logout() {
        String path = this.basePath + Api.APPLICATION_PATH + "/" + Api.LOGON;
        logger.trace("logout() - -- LOGOUT ------------");
        try(Response resp = this.deleteRequestPlain(path);) {
            boolean loggedOut = resp.readEntity(Boolean.class);
            logger.trace("logout() - loggedOut: {}", loggedOut);
        }
    }

    /**
     * Loads up to `limit` tempaltes from server.
     *
     * Used in live Matterial-App.
     */
    public ListResult<Document> loadTemplates(boolean loadRoleRights,
                                              boolean loadCategoriesPublicOnly,
                                              boolean loadCategories,
                                              boolean loadResponsibles,
                                              boolean loadAuthors,
                                              boolean loadLastAuthorOnly,
                                              boolean loadFollowers,
                                              boolean loadAmIFollowing,
                                              boolean loadMarkedAsHelpfulBy,
                                              boolean loadAttachments,
                                              boolean loadAdditionalProperties,
                                              boolean loadComments,
                                              int limit) {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.DOCUMENT;
        logger.debug("loadTemplates()");
        // *** load metadata of documents;
        ListResult<Document> lr = null;
        try(Response resp = this.get(path+
                                     "?"+Api.PARAM_SHOW_TEMPLATES_ONLY+"=true"+
                                     "&"+Api.PARAM_LOAD_ROLE_RIGHTS+"="+loadRoleRights+
                                     "&"+Api.PARAM_LOAD_CATEGORIES_PUBLIC_ONLY+"="+loadCategoriesPublicOnly+
                                     "&"+Api.PARAM_LOAD_CATEGORIES+"="+loadCategories+
                                     "&"+Api.PARAM_LOAD_RESPONSIBLES+"="+loadResponsibles+
                                     "&"+Api.PARAM_LOAD_AUTHORS+"="+loadAuthors+
                                     "&"+Api.PARAM_LOAD_LAST_AUTHOR_ONLY+"="+loadLastAuthorOnly+
                                     "&"+Api.PARAM_LOAD_FOLLOWERS+"="+loadFollowers+
                                     "&"+Api.PARAM_LOAD_AM_I_FOLLOWING+"="+loadAmIFollowing+
                                     "&"+Api.PARAM_LOAD_MARKED_AS_HELPFUL_BY+"="+loadMarkedAsHelpfulBy+
                                     "&"+Api.PARAM_LOAD_ATTACHMENTS+"="+loadAttachments+
                                     "&"+Api.PARAM_LOAD_ADDITIONAL_PROPERTIES+"="+loadAdditionalProperties+
                                     "&"+Api.PARAM_LOAD_COMMENTS+"="+loadComments+
                                     "&"+Api.PARAM_LIMIT+"="+limit+
                                     "&"+Api.PARAM_COUNT+"=true");) {
            lr = resp.readEntity(new GenericType<ListResult<Document>>() {});
        }
        return lr;
    }

    /**
     * Loads up to `limit` documents from server based on a `saved search id`.
     *
     * Used in live Matterial-App.
     */
    public ListResult<Document> loadDocumentsBySavedSearch(long savedSearchId,
                                                           boolean loadRoleRights,
                                                           boolean loadCategoriesPublicOnly,
                                                           boolean loadCategories,
                                                           boolean loadResponsibles,
                                                           boolean loadAuthors,
                                                           boolean loadLastAuthorOnly,
                                                           boolean loadFollowers,
                                                           boolean loadAmIFollowing,
                                                           boolean loadMarkedAsHelpfulBy,
                                                           boolean loadAttachments,
                                                           boolean loadAdditionalProperties,
                                                           boolean loadComments,
                                                           int limit) {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.DOCUMENT;
        logger.debug("loadDocumentsBySavedSearch()");
        // *** load metadata of documents;
        ListResult<Document> lr = null;
        try(Response resp = this.get(path+"/"+Api.SAVED_SEARCH+"/"+savedSearchId+"/"+Api.EXECUTE+
                                     "?"+Api.PARAM_LOAD_ROLE_RIGHTS+"="+loadRoleRights+
                                     "&"+Api.PARAM_LOAD_CATEGORIES_PUBLIC_ONLY+"="+loadCategoriesPublicOnly+
                                     "&"+Api.PARAM_LOAD_CATEGORIES+"="+loadCategories+
                                     "&"+Api.PARAM_LOAD_RESPONSIBLES+"="+loadResponsibles+
                                     "&"+Api.PARAM_LOAD_AUTHORS+"="+loadAuthors+
                                     "&"+Api.PARAM_LOAD_LAST_AUTHOR_ONLY+"="+loadLastAuthorOnly+
                                     "&"+Api.PARAM_LOAD_FOLLOWERS+"="+loadFollowers+
                                     "&"+Api.PARAM_LOAD_AM_I_FOLLOWING+"="+loadAmIFollowing+
                                     "&"+Api.PARAM_LOAD_MARKED_AS_HELPFUL_BY+"="+loadMarkedAsHelpfulBy+
                                     "&"+Api.PARAM_LOAD_ATTACHMENTS+"="+loadAttachments+
                                     "&"+Api.PARAM_LOAD_ADDITIONAL_PROPERTIES+"="+loadAdditionalProperties+
                                     "&"+Api.PARAM_LOAD_COMMENTS+"="+loadComments+
                                     "&"+Api.PARAM_LIMIT+"="+limit+
                                     "&"+Api.PARAM_COUNT+"=true");) {
            lr = resp.readEntity(new GenericType<ListResult<Document>>() {});
        }
        return lr;
    }

    /**
     * Loads the main content of a document.
     *
     * Used in live Matterial-App.
     */
    public String loadDocumentContent(long documentId, String languageKey) {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.DOCUMENT;
        logger.debug("loadDocumentContent({}, {})", documentId, languageKey);
        // *** load content;
        String content = null;
        try(Response resp = this.getNothing(path+"/"+documentId+"/"+Api.FILE+"?"+Api.PARAM_LANGUAGE_KEY+"="+languageKey);) {
            logger.trace("loadDocumentContent() - mediaType: {}", resp.getMediaType());
            logger.trace("loadDocumentContent() - length: {}", resp.getLength());
            try(InputStream in = resp.readEntity(InputStream.class)) {
                content = IOUtils.toString(in, CharEncoding.UTF_8);
            }
            catch (IOException e) {
                logger.error("loadDocumentContent() - ", e);
            }
        }
        return content;
    }

    /**
     * Loads the file content of an attachment.
     *
     * Used in live Matterial-App.
     */
    @SuppressWarnings("resource")
    public InputStream loadAttachmentContent(long documentId, long documentLanguageVersionId, long attachmentId) {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.DOCUMENT;
        logger.debug("loadAttachmentContent({}, {}, {})", documentId, documentLanguageVersionId, attachmentId);
        // *** load content;
        InputStream in = null;
        Response resp = this.getNothing(path+"/"+documentId+"/"+
                                            Api.VERSION+"/"+documentLanguageVersionId+"/"+
                                            Api.ATTACHMENT+"/"+attachmentId);
        logger.trace("loadAttachmentContent() - mediaType: {}", resp.getMediaType());
        logger.trace("loadAttachmentContent() - length: {}", resp.getLength());
        in = resp.readEntity(InputStream.class);
        return in;
    }

    /**
     * Loads all company infos from a matterial instance.
     *
     * Used in live Matterial-App.
     */
    public List<CompanyInfoItem> loadCompanyInfos() {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.CLIENT + "/" + Api.COMPANY_INFO;
        logger.debug("loadCompanyInfos()");
        // *** load all company infos;
        List<CompanyInfoItem> l = null;
        try(Response resp = this.get(path);) {
            l = resp.readEntity(new GenericType<List<CompanyInfoItem>>() {});
        }
        return l;
    }

    /**
     * Loads all roles by type from a matterial instance.
     *
     * Used in live Matterial-App.
     *
     * @param entityTypeIds for example: <code>Arrays.asList(EntityType.ROLE_CONTENT, EntityType.ROLE_FUNCTIONAL, EntityType.ROLE_REVIEW)</code>
     */
    public ListResult<Role> loadRolesByType(Set<Long> entityTypeIds) {
        String path = this.basePath+Api.APPLICATION_PATH+"/"+Api.ROLE+
                        "?"+Api.PARAM_COUNT+"=true";
        for (Long entityTypeId : entityTypeIds) {
            if(entityTypeId != null && entityTypeId > 0L) {
                path += "&"+Api.PARAM_ENTITY_TYPE_ID+"="+entityTypeId;
            }
        }
        logger.debug("loadRolesByType({})", entityTypeIds);
        // *** load roles;
        ListResult<Role> lr = null;
        try(Response resp = this.get(path);) {
            lr = resp.readEntity(new GenericType<ListResult<Role>>() {});
        }
        return lr;
    }

    /**
     * Loads all public categoryTypes from a matterial instance.
     *
     * Used in live Matterial-App.
     */
    public List<CategoryType> loadPublicCategoryTypes() {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.CATEGORY + "/" + Api.TYPE +
                        "?"+Api.PARAM_QUICK+"=false" +
                        "&"+Api.PARAM_PERSONAL+"=false";
        logger.debug("loadPublicCategoryTypes()");
        // *** load all category types;
        List<CategoryType> l = null;
        try(Response resp = this.get(path);) {
            l = resp.readEntity(new GenericType<List<CategoryType>>() {});
        }
        return l;
    }

    /**
     * Loads all public categories from a matterial instance.
     *
     * Used in live Matterial-App.
     */
    public List<Category> loadPublicCategories() {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.CATEGORY +
                        "?"+Api.PARAM_QUICK+"=false" +
                        "&"+Api.PARAM_PERSONAL+"=false";
        logger.debug("loadPublicCategoryTypes()");
        // *** load all category types;
        List<Category> l = null;
        try(Response resp = this.get(path);) {
            l = resp.readEntity(new GenericType<List<Category>>() {});
        }
        return l;
    }

    /**
     * Loads all custom additional properties from a matterial instance.
     *
     * Used in live Matterial-App.
     */
    public List<AdditionalProperty> loadCustomAdditionalProperties() {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.DOCUMENT + "/" + Api.ADDITIONAL_PROPERTY +
                        "?"+Api.PARAM_PROPERTY_TYPE+"=0";
        logger.debug("loadCustomAdditionalProperties()");
        // *** load all additional properties;
        List<AdditionalProperty> l = null;
        try(Response resp = this.get(path);) {
            l = resp.readEntity(new GenericType<List<AdditionalProperty>>() {});
        }
        return l;
    }

    /**
     * Loads the logo file content.
     *
     * Used in live Matterial-App.
     *
     * @param metaData tries to set keys `contentType` and `fileName` from response headers into given map
     */
    @SuppressWarnings("resource")
    public InputStream loadLogoContent(Map<String, String> metaData) {
        logger.debug("loadLogoContent()");
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.CLIENT + "/" + Api.LOGO;
        // *** load content;
        InputStream in = null;
        Response resp = this.getNothing(path);
        String contentType = resp.getHeaderString(HttpHeaders.CONTENT_TYPE);
        String contentDisposition = resp.getHeaderString(HttpHeaders.CONTENT_DISPOSITION);
        logger.trace("loadLogoContent() - contentType: {}", contentType);
        logger.trace("loadLogoContent() - contentDisposition: {}", contentDisposition);
        logger.trace("loadLogoContent() - length: {}", resp.getLength());
        String fileName = extractFileNameFromContentDisposition(contentDisposition);
        if(fileName == null) {
            fileName = "logo";
        }
        logger.trace("loadLogoContent() - fileName: {}", fileName);
        if(metaData != null) {
            metaData.put("fileName", fileName);
            metaData.put("contentType", contentType);
        }
        in = resp.readEntity(InputStream.class);
        return in;
    }

    private static final String extractFileNameFromContentDisposition(String contentDisposition) {
        String fileName = null;
        Pattern p = Pattern.compile("filename=\"(.+)\"");
        if(contentDisposition != null) {
            Matcher m = p.matcher(contentDisposition);
            if(m.find()) {
                fileName = m.group(1);
            }
        }
        return fileName;
    }

    /**
     * login which supports multiple servers (redirect to other server).
     * This invokes "preLogin", "redirectToLoginServer" (with the first instance found), and "loadLoginData".
     *
     * @return LoginData on a successfull login
     */
    public LoginData advancedLogin(String userName, String password) {
        List<DataSource> dataSources = this.preLogin(userName, password);
        LoginData ld = null;
        long instanceId = 0L;
        for (DataSource dataSource : dataSources) {
            logger.trace("advancedLogin() - FOUND DATA_SOURCE / INSTANCE: {} [{}]", dataSource.getDisplayName(), dataSource.getId());
            // *** selecting first instance to login;
            if(instanceId == 0L) {
                instanceId = dataSource.getId();
            }
        }
        if(this.redirectToLoginServer(instanceId)) {
            ld = this.loadLoginData();
        }
        return ld;
    }

    /**
     * "pre login" checks username and password and returns all available "instances" (DataSoources) for the given user.
     * After invoking "preLogin" a client should invoke "redirectToLoginServer" with the instanceId (dataSourceId).
     */
    public List<DataSource> preLogin(String userName, String password) {
        logger.trace("preLogin()");
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.LOGON + "/" + Api.PRE_LOGIN;
        List<DataSource> dataSources = null;
        Logon logon = new Logon(userName, password);
        try(Response resp = this.post(path, logon)) {
            this.updateSession(resp);
            dataSources = resp.readEntity(new GenericType<List<DataSource>>(){});
            logger.trace("preLogin() - sessionId: " + this.sessionId);
        }
        return dataSources;
    }

    /**
     * After a successful invocation of "preLogin" you can "login" via this method. This additionally redirects you to the correct server.
     * "instanceId" is the `DataSource.id` of the result of the "preLogin".
     */
    public boolean redirectToLoginServer(long instanceId) {
        logger.trace("redirectToLoginServer({})", instanceId);
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.LOGON + "/" + Api.LOGIN + "/" + instanceId;
        boolean success = false;
        // *** initiate redirection to the right server;
        try(Response resp = this.getNothing(path)) {
            int status = resp.getStatus();
            logger.trace("redirectToLoginServer() - status: {}", status);
            // *** status should be 303 with locationheader;
            if(status == 303) {
                String locationHeader = resp.getLocation().toString();
                logger.trace("redirectToLoginServer() - locationHeader: {}", locationHeader);
                // *** this location should look something like this: `http://server:8080/mtr-backend/api/logon/auto/xxx/xxx`
                // *** this actually logs you in!
                try(Response respLogin = this.getNothing(locationHeader)) {
                    status = respLogin.getStatus();
                    logger.trace("redirectToLoginServer() - statusLogin: {}", status);
                    String locationHeaderToUI = respLogin.getLocation().toString();
                    logger.trace("redirectToLoginServer() - locationHeaderToUI: {}", locationHeaderToUI);
                    // *** we ignore the last redirect to UI, because we are already logged-in and can continue using the api;
                    // *** on success this returns `303`;
                    if(status == 303) {
                        // *** we may have moved to another server;
                        this.updateBasePath(locationHeader);
                        success = true;
                    }
                }
            }
        }
        return success;
    }

    /**
     * Load the current LoginData (session data) for the current logged-in user.
     */
    public LoginData loadLoginData() {
        logger.trace("loadLoginData()");
        String pathLoginData = this.basePath + Api.APPLICATION_PATH + "/" + Api.LOGON + "/" + Api.LOGIN_DATA;
        LoginData ld = null;
        try(Response resp = this.get(pathLoginData)) {
            ld = resp.readEntity(LoginData.class);
            if(ld != null) {
                logger.trace("loadLoginData() - LOGGED IN PERSON: {}", ld.getPerson().toString());
            }
        }
        return ld;
    }


    /**
     * loads documents by (optional) languageKey via database request.
     */
    public ListResult<Document> loadDocuments(String languageKey,
                                              boolean loadRoleRights,
                                              boolean loadCategoriesPublicOnly,
                                              boolean loadCategories,
                                              boolean loadResponsibles,
                                              boolean loadAuthors,
                                              boolean loadLastAuthorOnly,
                                              boolean loadFollowers,
                                              boolean loadAmIFollowing,
                                              boolean loadMarkedAsHelpfulBy,
                                              boolean loadAttachments,
                                              boolean loadAdditionalProperties,
                                              boolean loadComments,
                                              String orderBy,
                                              String orderDir,
                                              int limit) {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.DOCUMENT;
        logger.debug("loadDocuments()");
        // *** load metadata of documents;
        ListResult<Document> lr = null;
        try(Response resp = this.get(path+
                                     "?"+Api.PARAM_ALL_LANGUAGES+"="+(languageKey==null)+
                                     ((languageKey != null)?
                                     "&"+Api.PARAM_LANGUAGE_KEY+"="+languageKey:"")+
                                     "&"+Api.PARAM_LOAD_ROLE_RIGHTS+"="+loadRoleRights+
                                     "&"+Api.PARAM_LOAD_CATEGORIES_PUBLIC_ONLY+"="+loadCategoriesPublicOnly+
                                     "&"+Api.PARAM_LOAD_CATEGORIES+"="+loadCategories+
                                     "&"+Api.PARAM_LOAD_RESPONSIBLES+"="+loadResponsibles+
                                     "&"+Api.PARAM_LOAD_AUTHORS+"="+loadAuthors+
                                     "&"+Api.PARAM_LOAD_LAST_AUTHOR_ONLY+"="+loadLastAuthorOnly+
                                     "&"+Api.PARAM_LOAD_FOLLOWERS+"="+loadFollowers+
                                     "&"+Api.PARAM_LOAD_AM_I_FOLLOWING+"="+loadAmIFollowing+
                                     "&"+Api.PARAM_LOAD_MARKED_AS_HELPFUL_BY+"="+loadMarkedAsHelpfulBy+
                                     "&"+Api.PARAM_LOAD_ATTACHMENTS+"="+loadAttachments+
                                     "&"+Api.PARAM_LOAD_ADDITIONAL_PROPERTIES+"="+loadAdditionalProperties+
                                     "&"+Api.PARAM_LOAD_COMMENTS+"="+loadComments+
                                     "&"+Api.PARAM_LIMIT+"="+limit+
                                     "&"+Api.PARAM_COUNT+"=true"+
                                     ((orderBy!=null)?
                                     "&"+Api.PARAM_ORDER_BY+"="+orderBy:"")+
                                     ((orderDir!=null)?
                                     "&"+Api.PARAM_ORDER_DIR+"="+orderDir:""));) {
            lr = resp.readEntity(new GenericType<ListResult<Document>>() {});
        }
        return lr;
    }

    /**
     * Creates a document with context token. The `contextToken` is needed as connection to the previously "uploaded" main-file and attachments.
     * This also directly tries to pulblish the new document.
     */
    public Document createDocumentAndPublish(Document document, String contextToken) {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.DOCUMENT;
        logger.trace("createDocumentAndPublish()");
        Document result = null;
        try(Response resp = this.post(path+
                                      "?"+Api.PARAM_PUBLISH_REQUEST+"="+Api.VALUE_PUBLISH_REQUEST_UNREVIEWED+
                                      ((contextToken != null)?
                                     "&"+Api.PARAM_CONTEXT_TOKEN+"="+contextToken:""),
                                      document)) {
            result = resp.readEntity(Document.class);
            logger.trace("createDocumentAndPublish() - result: {}", result);
        }
        return result;
    }

    /**
     * Updates a document with context token. The `contextToken` is needed as connection to the previously "uploaded" main-file and attachments.
     * This also directly tries to pulblish the new document.
     */
    public Document updateDocumentAndPublish(Document document, String contextToken) {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.DOCUMENT;
        logger.trace("updateDocumentAndPublish()");
        Document result = null;
        try(Response resp = this.put(path+"/"+document.getId()+
                                     "?"+Api.PARAM_PUBLISH_REQUEST+"="+Api.VALUE_PUBLISH_REQUEST_UNREVIEWED+
                                     ((contextToken != null)?
                                     "&"+Api.PARAM_CONTEXT_TOKEN+"="+contextToken:""),
                                     document)) {
            result = resp.readEntity(Document.class);
            logger.trace("updateDocumentAndPublish() - result: {}", result);
        }
        return result;
    }

    /**
     * Create a main file as tempfile (string). This should be done before storing a document.
     * Create or update a document after uploading the main-file with the same `contextToken` will connect the content to the document.
     */
    public TempFileDescriptor uploadTempFileAsString(String content, String contextToken, String languageKey) {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.TEMP_FILE;
        logger.trace("uploadTempFileAsString()");
        TempFileDescriptor result = null;
        try(Response resp = this.postPlain(path+
                                           "?"+Api.PARAM_CONTEXT_TOKEN+"="+contextToken+
                                           "&"+Api.PARAM_LANGUAGE_KEY+"="+languageKey,
                                           content)) {
            result = resp.readEntity(TempFileDescriptor.class);
            logger.trace("uploadTempFileAsString() - result: {}", result);
        }
        return result;
    }

    /**
     * Unlocks a document. You should always unlock the document after saving.
     */
    public Integer unlockDocument(long documentId) {
        String path = this.basePath + Api.APPLICATION_PATH+"/"+Api.DOCUMENT+"/"+Api.LOCK;
        logger.trace("unlockDocument()");
        Integer result = null;
        try(Response resp = this.deleteRequestPlain(path+"/"+documentId)) {
            result = resp.readEntity(Integer.class);
            logger.trace("unlockDocument() - result: {}", result);
        }
        return result;
    }

    /**
     * Using the integrated `flexmark` parser to get `markdown` from `html`.
     */
    public String htmlToMarkdown(String html) {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.CONVERSION+"/"+Api.HTML_2_MARKDOWN;
        logger.trace("htmlToMarkdown()");
        String result = null;
        try(Response resp = this.postHtmlRequestPlain(path, html)) {
            result = resp.readEntity(String.class);
            //logger.trace("htmlToMarkdown() - result: {}", result);
        }
        return result;
    }

    /**
     * Creates a person in current instance, but keep in mind, that these are no "accounts" and connot login.
     * You have to "invite" a `Credential` to create an account, that can login.
     */
    public Person createPerson(Person person) {
        String path = this.basePath + Api.APPLICATION_PATH+"/" + Api.PERSON;
        logger.trace("createPerson()");
        Person result = null;
        try(Response resp = this.post(path, person)) {
            result = resp.readEntity(Person.class);
            logger.trace("createPerson() - result: {}", result);
        }
        return result;
    }

}
