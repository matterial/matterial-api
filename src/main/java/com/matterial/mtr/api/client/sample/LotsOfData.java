package com.matterial.mtr.api.client.sample;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.matterial.mtr.api.client.MtrRestClient;
import com.matterial.mtr.api.object.Address;
import com.matterial.mtr.api.object.CommunicationData;
import com.matterial.mtr.api.object.Document;
import com.matterial.mtr.api.object.ListResult;
import com.matterial.mtr.api.object.LoginData;
import com.matterial.mtr.api.object.Person;
import com.matterial.mtr.api.object.Role;
import com.matterial.mtr.api.object.RoleRight;
import com.matterial.mtr.api.object.meta.EntityType;

/**
 * Sample client `AdvancedLogin` <br />
 * * login (simple) <br />
 * * creates some document from random wikipedia <br />
 */
public class LotsOfData {

    private static final Logger logger = LoggerFactory.getLogger(LotsOfData.class);

    private static List<String> firstNames = new ArrayList<>();
    private static List<String> lastNames = new ArrayList<>();

    /**
     * read some names from a text-file and store them into two separate lists for firstnames and lastnames.
     */
    public static void readNames() {
        try (Stream<String> stream = Files.lines( Paths.get("names.txt"), StandardCharsets.UTF_8)) {
            stream.forEach(line -> {
                if(!line.trim().isEmpty()) {
                    line = line.trim();
                    int pos = line.indexOf(" ");
                    String firstName = line.substring(0, pos).trim();
                    String lastName = line.substring(pos+1).trim();
                    logger.trace("readNames() - {} {}", firstName, lastName);
                    firstNames.add(firstName);
                    lastNames.add(lastName);
                }
            });
        }
        catch (IOException e) {
            logger.error("readNames()", e);
        }
    }

    /**
     * Create some random persons. As much as lastnames exists in name-file.
     */
    public static void createPersons(MtrRestClient client) {
        readNames();
        // *** create some persons;
        int index = 0;
        for(String lastName : lastNames) {
            Person p = new Person();
            p.setAccountLogin(UUID.randomUUID().toString());
            p.setFirstName(firstNames.get(index++));
            p.setLastName(lastName);
            p.setPosition("Position "+index);
            p.setGender((index % 2 > 0)?Person.GENDER_FEMALE:Person.GENDER_MALE);
            p.setBirthdayInSeconds(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));

            CommunicationData cd = new CommunicationData();
            cd.setEntityTypeId(EntityType.COMMUNICATION_DATA_EMAIL);
            cd.setValue(p.getFirstName()+"."+p.getLastName()+"@matterial.com");
            p.getCommunicationData().add(cd);

            Address addr = new Address();
            addr.setCity("Mainz");
            addr.setCountry("DE");
            addr.setHouseNumber(""+index);
            addr.setPostalCode("123456");
            addr.setStreet("Matterial Strasse");
            p.getAddresses().add(addr);

            logger.trace("person: {}", p);
            client.createPerson(p);
        }
    }

    /**
     * copy some random wiki content and create it as matterial document.
     * * creates (end - start) documents.
     * * adds the given contentRole with type EDIT.
     */
    public static void createDocuments(MtrRestClient client, int start, int end, String languageKey, long contentRoleId) {
        String wikiPath;
        String docTitle;
        if(languageKey.equals("en")) {
            wikiPath = "https://en.wikipedia.org/wiki/Special:Random";
            docTitle = "Wikipedia - Random Article ";
        }
        else if(languageKey.equals("fr")) {
            wikiPath = "https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Page_au_hasard";
            docTitle = "Wikipedia - Article au hasard ";
        }
        else if(languageKey.equals("it")) {
            wikiPath = "https://it.wikipedia.org/wiki/Speciale:PaginaCasuale";
            docTitle = "Wikipedia - Una voce a caso ";
        }
        else {
            languageKey = "de";
            wikiPath = "https://de.wikipedia.org/wiki/Spezial:Zuf%C3%A4llige_Seite";
            docTitle = "Wikipedia - Zufälliger Artikel ";
        }
        for(int i=start; i<=end;i++) {
            StringBuilder buffer = new StringBuilder();
            try {
                URL url = new URL(wikiPath);
                Scanner s = new Scanner(url.openStream());
                while(s.hasNext()) {
                    buffer.append(s.nextLine());
                }
                s.close();
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            String html = buffer.toString();
            String markdown = client.htmlToMarkdown(html);

            Document document = new Document();
            document.setLanguageVersionLanguageKey(languageKey);
            document.setLanguageVersionTitle(docTitle+i);
            document.setLanguageVersionAbstract("");

            RoleRight rr = new RoleRight();
            Role r = new Role();
            r.setId(contentRoleId);
            rr.setRole(r);
            rr.setType(RoleRight.EDIT);
            document.getRoleRights().add(rr);
            String contextToken = UUID.randomUUID().toString();
            client.uploadTempFileAsString(markdown, contextToken, document.getLanguageVersionLanguageKey());
            Document doc = client.createDocumentAndPublish(document, contextToken);
            client.unlockDocument(doc.getId());

            try {
                logger.info("createDocuments() - {} - {}/{}", languageKey, i, end);
                Thread.sleep(2000);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
    }

    /**
     *
     */
    public static void updateGroupOfDocuments(MtrRestClient client) {
        // *** get documents;
        ListResult<Document> lr = client.loadDocuments("en",   // languageKey
                                                       false,  // loadRoleRights
                                                       false,  // loadCategoriesPublicOnly
                                                       false,  // loadCategories
                                                       false,  // loadResponsibles
                                                       false,  // loadAuthors
                                                       false,  // loadLastAuthorOnly
                                                       false,  // loadFollowers
                                                       false,  // loadAmIFollowing
                                                       false,  // loadMarkedAsHelpfulBy
                                                       true,   // loadAttachments
                                                       true,   // loadAdditionalProperties
                                                       false,  // loadComments
                                                       "createTimeInSeconds", // orderBy
                                                       "asc",  // orderDir
                                                       300     // limit
                                                       );
        List<Document> docs = lr.getResults();
        long count = lr.getTotalHits();
        logger.info("COUNT OF DOCUMENTS: {}", count);
        for (Document document : docs) {
            logger.info("TITLE: {}", document.getLanguageVersionTitle());
            if(document.getLanguageVersionTitle().startsWith("Wikipedia")) {

                RoleRight rr = new RoleRight();
                Role r = new Role();
                r.setId(5L); // Matterial;
                rr.setRole(r);
                rr.setType(RoleRight.EDIT);
                document.getRoleRights().add(rr);

                client.updateDocumentAndPublish(document, null);
            }
        }
    }

    /**
     * sample-usage;
     */
    public static void main(String[] args) throws InterruptedException {
        String matterialPath = MtrRestClient.PATH_LIVE;
        String userName = "user";
        String passwd = "***";
        if(args != null) {
            int i=0;
            for (String arg : args) {
                if(i==0) {
                    matterialPath = arg;
                }
                else if(i==1) {
                    userName = arg;
                }
                else if(i==2) {
                    passwd = arg;
                }
                i++;
            }
        }
        System.out.println("Sample Matterial Client: LotsOfData");
        System.out.println("Using matterialPath: "+matterialPath);
        System.out.println("Using userName: "+userName);

        MtrRestClient client = new MtrRestClient(matterialPath);
        LoginData ld = client.simpleLogin(userName, passwd);
        logger.info("CURRENT DS: "+ld.getCurrentDataSource().getDisplayName());

        // *** create some persons;
        //createPersons(client);

        // *** create some documents;
        createDocuments(client, 1, 10, "de", 5L);
        createDocuments(client, 1, 10, "en", 5L);

        client.logout();
    }

}
