package com.matterial.mtr.api.object;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>PreferenceNotification</strong>
 */
@XmlRootElement
public class PreferenceNotification extends Notification {

    private static final long serialVersionUID = 1L;

    public static final String PREFERENCE_RELOAD = "PREFERENCE_RELOAD";

    /**
     * reload all.
     */
    public PreferenceNotification() {
        super(PREFERENCE_RELOAD);
    }

    /**
     * reload one preferenceKey only.
     */
    public PreferenceNotification(String message) {
        super(message);
    }

}
