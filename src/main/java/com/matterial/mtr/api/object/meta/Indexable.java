package com.matterial.mtr.api.object.meta;

import java.util.List;

/**
 * Interface for classes to be indexable by the EmbeddedSearchServer
 */
public abstract class Indexable extends IndexableChild {

    private static final long serialVersionUID = 1L;

    public static final String INDEX_FIELD_INDEX_TYPE_NAME = "indexTypeName";
    public static final String INDEX_FIELD_AUTOCOMPLETE = "autocomplete";

    /**
     * Get id. Should be always be prefixed with indexTypeName!
     */
    public abstract String indexId();

    /**
     * Get the typeName to be used for indexing
     */
    public String indexTypeName() {
        return this.getJsonType().toLowerCase();
    }

    /**
     * Textual information to be used for autocomplete<br/>
     * Be aware of memory consumption
     */
    public abstract String autocomplete();

    public List<String> autocompleteAdditionalIndexFields() {
        return null;
    }

    public boolean isPartialUpdate() {
        return false;
    }

    public void setPartialUpdate(boolean partialUpdate) {
        // *** do nothing;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\nindexTypeName: ");
        buffer.append(this.indexTypeName());
        buffer.append(", \npartialUpdate: ");
        buffer.append(this.isPartialUpdate());
        return buffer.toString();
    }

}
