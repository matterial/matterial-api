package com.matterial.mtr.api.object;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>LicenceUsage</strong>
 */
@XmlRootElement
public class LicenceUsage implements Serializable {

    private static final long serialVersionUID = 1L;

    private int user;
    private int userLimited;
    private int casSize;
    private Long dataSourceActiveUntilInSeconds;
    private Licence licence;

    public LicenceUsage() {
        // *** do nothing;
    }

    public LicenceUsage(int user,
                        int userLimited,
                        int casSize,
                        Long dataSourceActiveUntilInSeconds,
                        Licence licence) {
        this.user = user;
        this.userLimited = userLimited;
        this.casSize = casSize;
        this.dataSourceActiveUntilInSeconds = dataSourceActiveUntilInSeconds;
        this.licence = licence;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getUserLimited() {
        return userLimited;
    }

    public void setUserLimited(int userLimited) {
        this.userLimited = userLimited;
    }

    public int getCasSize() {
        return casSize;
    }

    public void setCasSize(int casSize) {
        this.casSize = casSize;
    }

    public Long getDataSourceActiveUntilInSeconds() {
        return dataSourceActiveUntilInSeconds;
    }

    public void setDataSourceActiveUntilInSeconds(Long dataSourceActiveUntilInSeconds) {
        this.dataSourceActiveUntilInSeconds = dataSourceActiveUntilInSeconds;
    }

    public Licence getLicence() {
        return licence;
    }

    public void setLicence(Licence licence) {
        this.licence = licence;
    }

    /**
     * checks if new licence exceeds current values.
     */
    public boolean checkForExceeding(Licence newLicence) {
        boolean ok = false;
        // *** get current values;
        int currentCasSize = this.getCasSize();
        int currentUser = this.getUser();
        int currentUserLimited = this.getUserLimited();
        // *** check licence;
        if(newLicence != null) {
            // *** get requested values;
            int requestedCasSize = newLicence.getCasSize();
            int requestedUser = newLicence.getUser();
            int requestedUserLimited = newLicence.getUserLimited();
            if(requestedCasSize >= currentCasSize &&
               requestedUser >= currentUser &&
               requestedUserLimited >= currentUserLimited) {
                ok = true;
            }
        }
        return ok;
    }

}
