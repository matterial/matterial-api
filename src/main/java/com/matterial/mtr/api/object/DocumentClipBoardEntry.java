package com.matterial.mtr.api.object;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>DocumentClipBoardEntry</strong>
 */
@XmlRootElement
public class DocumentClipBoardEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    public final static int MAX_CLIP_BOARD_SIZE = 1000;

    /** document.id */
    private long id;
    /** document.languageVersionLanguageId */
    private long languageVersionLanguageId;

    public DocumentClipBoardEntry() {
        // *** do nothing;
    }

    public DocumentClipBoardEntry(long id, long languageVersionLanguageId) {
        this.id = id;
        this.languageVersionLanguageId = languageVersionLanguageId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLanguageVersionLanguageId() {
        return languageVersionLanguageId;
    }

    public void setLanguageVersionLanguageId(long languageVersionLanguageId) {
        this.languageVersionLanguageId = languageVersionLanguageId;
    }

}
