package com.matterial.mtr.api.object;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.matterial.mtr.api.object.meta.Convertible;
import com.matterial.mtr.api.object.meta.ConvertibleEnhanced;
import com.matterial.mtr.api.object.meta.Identifiable;
import com.matterial.mtr.api.object.meta.IndexableChild;

/**
 * <strong>Client</strong>
 */
@XmlRootElement
public class Client extends IndexableChild implements Identifiable, Convertible, ConvertibleEnhanced {

    private static final long serialVersionUID = 1L;

    public static final String DEFAULT_NAME = "DEFAULT";

    public static final String INDEX_FIELD_ID = "id";
    public static final String INDEX_FIELD_NAME = "name";

    public static final String INDEX_FIELD_CAS_ID = "casId";
    public static final String INDEX_FIELD_CAS_ID_IMG_SMALL = "casIdImgSmall";
    public static final String INDEX_FIELD_CAS_ID_IMG_LARGE = "casIdImgLarge";
    public static final String INDEX_FIELD_CAS_ID_THUMBNAIL = "casIdThumbnail";
    public static final String INDEX_FIELD_FILE_SIZE = "fileSize";
    public static final String INDEX_FIELD_FILE_SIZE_IMG_SMALL = "fileSizeImgSmall";
    public static final String INDEX_FIELD_FILE_SIZE_IMG_LARGE = "fileSizeImgLarge";
    public static final String INDEX_FIELD_FILE_SIZE_THUMBNAIL = "fileSizeThumbnail";
    public static final String INDEX_FIELD_SKIN_CLOUR_BUTTON = "skinColourButton";
    public static final String INDEX_FIELD_SKIN_CLOUR_NOTIFICATION = "skinColourNotification";
    public static final String INDEX_FIELD_SKIN_CLOUR_LABEL = "skinColourLabel";

    private long id;
    private String name;
    private String casId;
    private String casIdImgSmall;
    private String casIdImgLarge;
    private String casIdThumbnail;
    private Long fileSize;
    private Long fileSizeImgSmall;
    private Long fileSizeImgLarge;
    private Long fileSizeThumbnail;
    private String skinColourButton;
    private String skinColourNotification;
    private String skinColourLabel;

    private PreferenceMapContainer clientPreferences;
    private List<CompanyInfoItem> companyInfoItems;

    public Client() {
        // *** do nothing;
    }

    public Client(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Client(long id,
                  String name,
                  String casId,
                  String casIdImgSmall,
                  String casIdImgLarge,
                  String casIdThumbnail,
                  Long fileSize,
                  Long fileSizeImgSmall,
                  Long fileSizeImgLarge,
                  Long fileSizeThumbnail,
                  String skinColourButton,
                  String skinColourNotification,
                  String skinColourLabel) {
        this(id, name);
        this.casId = casId;
        this.casIdImgSmall = casIdImgSmall;
        this.casIdImgLarge = casIdImgLarge;
        this.casIdThumbnail = casIdThumbnail;
        this.fileSize = fileSize;
        this.fileSizeImgSmall = fileSizeImgSmall;
        this.fileSizeImgLarge = fileSizeImgLarge;
        this.fileSizeThumbnail = fileSizeThumbnail;
        this.skinColourButton = skinColourButton;
        this.skinColourNotification = skinColourNotification;
        this.skinColourLabel = skinColourLabel;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getCasId() {
        return casId;
    }

    public void setCasId(String casId) {
        this.casId = casId;
    }

    @Override
    public String getCasIdImgSmall() {
        return casIdImgSmall;
    }

    public void setCasIdImgSmall(String casIdImgSmall) {
        this.casIdImgSmall = casIdImgSmall;
    }

    @Override
    public String getCasIdImgLarge() {
        return casIdImgLarge;
    }

    public void setCasIdImgLarge(String casIdImgLarge) {
        this.casIdImgLarge = casIdImgLarge;
    }

    @Override
    public String getCasIdThumbnail() {
        return casIdThumbnail;
    }

    public void setCasIdThumbnail(String casIdThumbnail) {
        this.casIdThumbnail = casIdThumbnail;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Long getFileSizeImgSmall() {
        return fileSizeImgSmall;
    }

    public void setFileSizeImgSmall(Long fileSizeImgSmall) {
        this.fileSizeImgSmall = fileSizeImgSmall;
    }

    public Long getFileSizeImgLarge() {
        return fileSizeImgLarge;
    }

    public void setFileSizeImgLarge(Long fileSizeImgLarge) {
        this.fileSizeImgLarge = fileSizeImgLarge;
    }

    public Long getFileSizeThumbnail() {
        return fileSizeThumbnail;
    }

    public void setFileSizeThumbnail(Long fileSizeThumbnail) {
        this.fileSizeThumbnail = fileSizeThumbnail;
    }

    public String getSkinColourButton() {
        return skinColourButton;
    }

    public void setSkinColourButton(String skinColourButton) {
        this.skinColourButton = skinColourButton;
    }

    public String getSkinColourNotification() {
        return skinColourNotification;
    }

    public void setSkinColourNotification(String skinColourNotification) {
        this.skinColourNotification = skinColourNotification;
    }

    public String getSkinColourLabel() {
        return skinColourLabel;
    }

    public void setSkinColourLabel(String skinColourLabel) {
        this.skinColourLabel = skinColourLabel;
    }

    public PreferenceMapContainer getClientPreferences() {
        if (this.clientPreferences == null) {
            this.clientPreferences = new PreferenceMapContainer();
        }
        return clientPreferences;
    }

    public void setClientPreferences(PreferenceMapContainer clientPreferences) {
        this.clientPreferences = clientPreferences;
    }

    public List<CompanyInfoItem> getCompanyInfoItems() {
        if(this.companyInfoItems == null) {
            this.companyInfoItems = new ArrayList<>();
        }
        return companyInfoItems;
    }

    public void setCompanyInfoItems(List<CompanyInfoItem> companyInfoItems) {
        this.companyInfoItems = companyInfoItems;
    }

    @Override
    public Map<String, Object> indexMap() {
        // *** creating index-map;
        // *** only indexing `id` and `name`;
        Map<String, Object> indexMap = this.indexMap(Arrays.asList(INDEX_FIELD_CAS_ID,
                                                                   INDEX_FIELD_CAS_ID_IMG_SMALL,
                                                                   INDEX_FIELD_CAS_ID_IMG_LARGE,
                                                                   INDEX_FIELD_CAS_ID_THUMBNAIL,
                                                                   INDEX_FIELD_FILE_SIZE,
                                                                   INDEX_FIELD_FILE_SIZE_IMG_SMALL,
                                                                   INDEX_FIELD_FILE_SIZE_IMG_LARGE,
                                                                   INDEX_FIELD_FILE_SIZE_THUMBNAIL,
                                                                   INDEX_FIELD_SKIN_CLOUR_BUTTON,
                                                                   INDEX_FIELD_SKIN_CLOUR_NOTIFICATION,
                                                                   INDEX_FIELD_SKIN_CLOUR_LABEL));
        return indexMap;
    }

}
