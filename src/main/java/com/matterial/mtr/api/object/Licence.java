package com.matterial.mtr.api.object;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>Licence</strong>
 */
@XmlRootElement
public class Licence implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String MTR_PREFIX = "MTR-";

    public static final long LICENCE_ID_MTR_USER = 1L;
    public static final long LICENCE_ID_MTR_CAS_SIZE = 2L;
    //public static final long LICENCE_ID_MTR_PACKAGE_GROUP = 3L;
    //public static final long LICENCE_ID_MTR_PACKAGE_ROLE = 4L;
    public static final long LICENCE_ID_MTR_PACKAGE_REVIEW = 5L;
    //public static final long LICENCE_ID_MTR_PACKAGE_MULTI_LANGUAGE = 6L;
    public static final long LICENCE_ID_MTR_USER_LIMITED = 7L;

    private static final String MTR_USER = "USER";
    private static final String MTR_USER_LIMITED = "USER_LIMITED";
    private static final String MTR_CAS_SIZE = "CAS_SIZE";
    private static final String MTR_PACKAGE_REVIEW = "PACKAGE_REVIEW";

    public static final int MTR_USER_DEFAULT = 1;
    public static final int MTR_CAS_SIZE_DEFAULT = 100;

    public static final long MB = (1024L*1024L);

    private int user;
    private int userLimited;
    private int casSize;
    private boolean packageReview;
    private String hash;

    public Licence() {
        this.user = MTR_USER_DEFAULT;
        this.userLimited = 0;
        this.casSize = MTR_CAS_SIZE_DEFAULT;
        this.packageReview = false;
        this.hash = null;
    }

    public Licence(int user,
                   int userLimited,
                   int casSize,
                   boolean packageReview) {
        this();
        if(user > 0) {
            this.user = user;
        }
        if(userLimited > 0) {
            this.userLimited = userLimited;
        }
        if(casSize > 0) {
            this.casSize = casSize;
        }
        this.packageReview = packageReview;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getUserLimited() {
        return userLimited;
    }

    public void setUserLimited(int userLimited) {
        this.userLimited = userLimited;
    }

    public int getCasSize() {
        return casSize;
    }

    public void setCasSize(int casSize) {
        this.casSize = casSize;
    }

    public boolean isPackageReview() {
        return packageReview;
    }

    public void setPackageReview(boolean packageReview) {
        this.packageReview = packageReview;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    /**
     * this is used to generate the license-key!
     */
    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        // *** add prefix;
        buffer.append(MTR_PREFIX);
        // *** always add user;
        buffer.append(MTR_USER);
        buffer.append("-");
        buffer.append(this.getUser());
        // *** cas-size;
        if(this.getUserLimited() > 0) {
            buffer.append("-");
            buffer.append(MTR_USER_LIMITED);
            buffer.append("-");
            buffer.append(this.getUserLimited());
        }
        // *** cas-size;
        if(this.getCasSize() > 0) {
            buffer.append("-");
            buffer.append(MTR_CAS_SIZE);
            buffer.append("-");
            buffer.append(this.getCasSize());
        }
        // *** review;
        if(this.isPackageReview()) {
            buffer.append("-");
            buffer.append(MTR_PACKAGE_REVIEW);
            buffer.append("-");
            buffer.append(this.isPackageReview());
        }
        return buffer.toString();
    }

}
