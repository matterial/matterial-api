package com.matterial.mtr.api.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PersonClientContainerList implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<PersonClientContainer> personClientContainers;

    public List<PersonClientContainer> getPersonClientContainers() {
        if(this.personClientContainers == null) {
            this.personClientContainers = new ArrayList<>();
        }
        return personClientContainers;
    }


    public void setPersonClientContainers(List<PersonClientContainer> personClientContainers) {
        this.personClientContainers = personClientContainers;
    }

}
