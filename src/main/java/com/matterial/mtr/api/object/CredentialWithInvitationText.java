package com.matterial.mtr.api.object;

/**
 * <strong>CredentialWithInvitationText</strong>
 */
public class CredentialWithInvitationText extends Credential {

    private static final long serialVersionUID = 1L;

    private String invitationText;

    public String getInvitationText() {
        return invitationText;
    }

    public void setInvitationText(String invitationText) {
        this.invitationText = invitationText;
    }

}
