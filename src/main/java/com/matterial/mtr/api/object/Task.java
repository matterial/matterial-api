package com.matterial.mtr.api.object;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import javax.xml.bind.annotation.XmlRootElement;

import com.matterial.mtr.api.object.meta.Identifiable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Container representing a task
 */
@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@XmlRootElement
public class Task extends ListResultEntry implements Identifiable {

    private static final long serialVersionUID = 1L;

    public static final int RESUBMISSION_TIME_UNIT_DAYS = 0;
    public static final int RESUBMISSION_TIME_UNIT_MONTHS = 1;

    private long id;
    private long creationDateInSeconds;
    private String description;
    private String message;
    private Long dueDateInSeconds;
    private boolean overdue;
    private Integer resubmissionTimePeriod;
    private Integer resubmissionTimeUnit;
    private Long resubmissionDateInSeconds;
    private boolean snap;

    private Long documentId;
    private Long documentLanguageVersionId;
    private Integer documentLanguageVersionVersion;
    private String documentLanguageVersionTitle;
    private String documentLanguageVersionLanguageKey;
    private long taskStatusId;

    private Role assignedRole;
    private Person acceptedAccount;
    private Person authorAccount;

    public Task(long id,
                Integer resubmissionTimePeriod,
                Integer resubmissionTimeUnit) {
        this(id,
             0L, // creationDateInSeconds
             null, // description
             null, // message
             null, // dueDateInSeconds
             resubmissionTimePeriod,
             resubmissionTimeUnit,
             null, // resubmissionDateInSeconds
             false, // snap
             null, // documentId
             null, // documentLanguageVersionId
             null, // documentLanguageVersionVersion
             null, // documentLanguageVersionTitle
             null, // documentLanguageVersionLanguageKey
             0L, // taskStatusId
             null, // assignedRoleId
             null // assignedAccountId
             );
    }

    public Task(long id,
                long creationDateInSeconds,
                String description,
                String message,
                Long dueDateInSeconds,
                Integer resubmissionTimePeriod,
                Integer resubmissionTimeUnit,
                Long resubmissionDateInSeconds,
                boolean snap,
                Long documentId,
                Long documentLanguageVersionId,
                Integer documentLanguageVersionVersion,
                String documentLanguageVersionTitle,
                String documentLanguageVersionLanguageKey,
                long taskStatusId,
                // *** assigned-role;
                Long assignedRoleId,
                // *** assigned-account;
                Long assignedAccountId) {
        this(id, creationDateInSeconds, description, message, dueDateInSeconds, resubmissionTimePeriod, resubmissionTimeUnit, resubmissionDateInSeconds, snap,
             documentId, documentLanguageVersionId, documentLanguageVersionVersion, documentLanguageVersionTitle, documentLanguageVersionLanguageKey, taskStatusId,
             // *** assigned-role;
             assignedRoleId, null, null, null, null, null, null, null, null,
             // *** assigned-account;
             assignedAccountId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    public Task(long id,
                long creationDateInSeconds,
                String description,
                String message,
                Long dueDateInSeconds,
                Integer resubmissionTimePeriod,
                Integer resubmissionTimeUnit,
                Long resubmissionDateInSeconds,
                boolean snap,
                Long documentId,
                Long documentLanguageVersionId,
                Integer documentLanguageVersionVersion,
                String documentLanguageVersionTitle,
                String documentLanguageVersionLanguageKey,
                long taskStatusId,
                // *** assigned-role;
                Long assignedRoleId,
                Long assignedRoleClientId,
                Long assignedRoleEntityTypeId,
                String assignedRoleName,
                String assignedRoleDescription,
                Long assignedRoleBitmask,
                Boolean assignedRoleNotRemovable,
                Boolean assignedRoleInitiallyAssignedToAccount,
                Long assignedRoleInitiallyAssignedTypeToDocument,
                // *** assigned-account;
                Long acceptedAccountId,
                String acceptedAccountLogin,
                Integer acceptedAccountStatus,
                Long acceptedAccountSuperiorAccountId,
                Long acceptedAccountCreateTimeInSeconds,
                Long acceptedAccountLastLoginInSeconds,
                Boolean acceptedAccountInstanceAdmin,
                Boolean acceptedAccountDemo,
                Boolean acceptedAccountLimited,
                Boolean acceptedAccountActive,
                Long acceptedAccountContactId,
                String acceptedAccountFirstName,
                String acceptedAccountLastName,
                String acceptedAccountPosition,
                Long acceptedAccountBirthdayInSeconds,
                Integer acceptedAccountGender) {
        this(id, creationDateInSeconds, description, message, dueDateInSeconds, resubmissionTimePeriod, resubmissionTimeUnit, resubmissionDateInSeconds, snap,
             documentId, documentLanguageVersionId, documentLanguageVersionVersion, documentLanguageVersionTitle, documentLanguageVersionLanguageKey, taskStatusId,
             // *** assigned-role;
             assignedRoleId, assignedRoleClientId, assignedRoleEntityTypeId,
             assignedRoleName, assignedRoleDescription, assignedRoleBitmask,
             assignedRoleNotRemovable, assignedRoleInitiallyAssignedToAccount, assignedRoleInitiallyAssignedTypeToDocument,
             // *** accepted-account;
             acceptedAccountId, acceptedAccountLogin,
             acceptedAccountSuperiorAccountId,
             acceptedAccountCreateTimeInSeconds,
             acceptedAccountLastLoginInSeconds, acceptedAccountInstanceAdmin, acceptedAccountDemo, acceptedAccountLimited, acceptedAccountActive,
             acceptedAccountContactId, acceptedAccountFirstName, acceptedAccountLastName,
             acceptedAccountPosition, acceptedAccountBirthdayInSeconds, acceptedAccountGender,
             // *** skip: author-account;
             null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    public Task(long id,
                long creationDateInSeconds,
                String description,
                String message,
                Long dueDateInSeconds,
                Integer resubmissionTimePeriod,
                Integer resubmissionTimeUnit,
                Long resubmissionDateInSeconds,
                boolean snap,
                Long documentId,
                Long documentLanguageVersionId,
                Integer documentLanguageVersionVersion,
                String documentLanguageVersionTitle,
                String documentLanguageVersionLanguageKey,
                long taskStatusId,
                // *** assigned-role;
                Long assignedRoleId,
                Long assignedRoleClientId,
                Long assignedRoleEntityTypeId,
                String assignedRoleName,
                String assignedRoleDescription,
                Long assignedRoleBitmask,
                Boolean assignedRoleNotRemovable,
                Boolean assignedRoleInitiallyAssignedToAccount,
                Long assignedRoleInitiallyAssignedTypeToDocument,
                // *** accepted-account;
                Long acceptedAccountId,
                String acceptedAccountLogin,
                Long acceptedAccountSuperiorAccountId,
                Long acceptedAccountCreateTimeInSeconds,
                Long acceptedAccountLastLoginInSeconds,
                Boolean acceptedAccountInstanceAdmin,
                Boolean acceptedAccountDemo,
                Boolean acceptedAccountLimited,
                Boolean acceptedAccountActive,
                Long acceptedAccountContactId,
                String acceptedAccountFirstName,
                String acceptedAccountLastName,
                String acceptedAccountPosition,
                Long acceptedAccountBirthdayInSeconds,
                Integer acceptedAccountGender,
                // *** author-account;
                Long authorAccountId,
                String authorAccountLogin,
                Long authorAccountSuperiorAccountId,
                Long authorAccountCreateTimeInSeconds,
                Long authorAccountLastLoginInSeconds,
                Boolean authorAccountInstanceAdmin,
                Boolean authorAccountDemo,
                Boolean authorAccountLimited,
                Boolean authorAccountActive,
                Long authorAccountContactId,
                String authorAccountFirstName,
                String authorAccountLastName,
                String authorAccountPosition,
                Long authorAccountBirthdayInSeconds,
                Integer authorAccountGender) {
        this.id = id;
        this.creationDateInSeconds = creationDateInSeconds;
        this.description = description;
        this.message = message;
        this.dueDateInSeconds = dueDateInSeconds;
        this.resubmissionTimePeriod = resubmissionTimePeriod;
        this.resubmissionTimeUnit = resubmissionTimeUnit;
        this.resubmissionDateInSeconds = resubmissionDateInSeconds;
        this.snap = snap;
        this.documentId = documentId;
        this.documentLanguageVersionId = documentLanguageVersionId;
        this.documentLanguageVersionVersion = documentLanguageVersionVersion;
        this.documentLanguageVersionTitle = documentLanguageVersionTitle;
        this.documentLanguageVersionLanguageKey = documentLanguageVersionLanguageKey;
        this.taskStatusId = taskStatusId;
        // *** assigned-role;
        if(assignedRoleId != null && assignedRoleId > 0L &&
           assignedRoleEntityTypeId != null && assignedRoleBitmask != null &&
           assignedRoleNotRemovable != null &&
           assignedRoleInitiallyAssignedToAccount != null &&
           assignedRoleInitiallyAssignedTypeToDocument != null) {
            this.assignedRole = new Role(assignedRoleId, assignedRoleClientId, assignedRoleEntityTypeId,
                                         assignedRoleName, assignedRoleDescription, assignedRoleBitmask,
                                         assignedRoleNotRemovable, assignedRoleInitiallyAssignedToAccount,
                                         assignedRoleInitiallyAssignedTypeToDocument);
        }
        else if(assignedRoleId != null && assignedRoleId > 0L) {
            this.assignedRole = new Role();
            this.assignedRole.setId(assignedRoleId);
        }
        // *** accepted-account;
        if(acceptedAccountId != null && acceptedAccountId > 0L &&
           acceptedAccountContactId != null) {
            this.acceptedAccount = new Person(acceptedAccountId, acceptedAccountLogin,
                                              acceptedAccountSuperiorAccountId,
                                              acceptedAccountCreateTimeInSeconds,
                                              acceptedAccountLastLoginInSeconds,
                                              acceptedAccountInstanceAdmin,
                                              acceptedAccountDemo,
                                              acceptedAccountLimited,
                                              acceptedAccountActive,
                                              acceptedAccountContactId, acceptedAccountFirstName, acceptedAccountLastName,
                                              acceptedAccountPosition, acceptedAccountBirthdayInSeconds, acceptedAccountGender);
        }
        else if(acceptedAccountId != null && acceptedAccountId > 0L) {
            this.acceptedAccount = new Person();
            this.acceptedAccount.setAccountId(acceptedAccountId);
        }
        // *** author-account;
        if(authorAccountId != null && authorAccountId > 0L &&
           authorAccountContactId != null) {
            this.authorAccount = new Person(authorAccountId, authorAccountLogin,
                                            authorAccountSuperiorAccountId,
                                            authorAccountCreateTimeInSeconds,
                                            authorAccountLastLoginInSeconds,
                                            authorAccountInstanceAdmin,
                                            authorAccountDemo,
                                            authorAccountLimited,
                                            authorAccountActive,
                                            authorAccountContactId, authorAccountFirstName, authorAccountLastName,
                                            authorAccountPosition, authorAccountBirthdayInSeconds, authorAccountGender);
        }
        else if(authorAccountId != null && authorAccountId > 0L) {
            this.authorAccount = new Person();
            this.authorAccount.setAccountId(authorAccountId);
        }
        // *** calculate overdue;
        // *** overdue only if closed / rejected;
        if(taskStatusId != TaskStatus.ID_CLOSED &&
           taskStatusId != TaskStatus.ID_REJECTED &&
           this.dueDateInSeconds != null ) {
            // *** overdue, if dueDateInSeconds < now;
            long now = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
            this.overdue = (this.dueDateInSeconds < now);
        }
    }

}