package com.matterial.mtr.api.object.meta;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.matterial.mtr.api.object.Activity;
import com.matterial.mtr.api.object.AdditionalProperty;
import com.matterial.mtr.api.object.Address;
import com.matterial.mtr.api.object.Category;
import com.matterial.mtr.api.object.CategoryNotification;
import com.matterial.mtr.api.object.Client;
import com.matterial.mtr.api.object.Comment;
import com.matterial.mtr.api.object.CommunicationData;
import com.matterial.mtr.api.object.ContactImage;
import com.matterial.mtr.api.object.DataSource;
import com.matterial.mtr.api.object.Document;
import com.matterial.mtr.api.object.DocumentChangeLog;
import com.matterial.mtr.api.object.DocumentNotification;
import com.matterial.mtr.api.object.ExtensionValue;
import com.matterial.mtr.api.object.IndexNotification;
import com.matterial.mtr.api.object.LoginDataNotification;
import com.matterial.mtr.api.object.Notification;
import com.matterial.mtr.api.object.Person;
import com.matterial.mtr.api.object.PreferenceNotification;
import com.matterial.mtr.api.object.Role;
import com.matterial.mtr.api.object.RoleRight;
import com.matterial.mtr.api.object.SearchResultEntry;
import com.matterial.mtr.api.object.SimpleValue;
import com.matterial.mtr.api.object.Task;
import com.matterial.mtr.api.object.TrackingItem;

/**
 * <strong>ApiObjectWithJsonType</strong>
 */
@XmlRootElement
@XmlSeeAlso({
    Activity.class,
    AdditionalProperty.class,
    Address.class,
    Client.class,
    Category.class,
    CommunicationData.class,
    Comment.class,
    ContactImage.class,
    DataSource.class,
    Document.class,
    DocumentChangeLog.class,
    ExtensionValue.class,
    Person.class,
    Role.class,
    RoleRight.class,
    SearchResultEntry.class,
    SimpleValue.class,
    Task.class,
    TrackingItem.class,
    Notification.class,
    CategoryNotification.class,
    DocumentNotification.class,
    IndexNotification.class,
    LoginDataNotification.class,
    PreferenceNotification.class
})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
     include = As.EXISTING_PROPERTY,
     property = "jsonType")
@JsonSubTypes({
      @JsonSubTypes.Type(Activity.class),
      @JsonSubTypes.Type(AdditionalProperty.class),
      @JsonSubTypes.Type(Address.class),
      @JsonSubTypes.Type(Client.class),
      @JsonSubTypes.Type(Category.class),
      @JsonSubTypes.Type(CommunicationData.class),
      @JsonSubTypes.Type(Comment.class),
      @JsonSubTypes.Type(ContactImage.class),
      @JsonSubTypes.Type(DataSource.class),
      @JsonSubTypes.Type(Document.class),
      @JsonSubTypes.Type(DocumentChangeLog.class),
      @JsonSubTypes.Type(ExtensionValue.class),
      @JsonSubTypes.Type(Person.class),
      @JsonSubTypes.Type(Role.class),
      @JsonSubTypes.Type(RoleRight.class),
      @JsonSubTypes.Type(SearchResultEntry.class),
      @JsonSubTypes.Type(SimpleValue.class),
      @JsonSubTypes.Type(Task.class),
      @JsonSubTypes.Type(TrackingItem.class),
      @JsonSubTypes.Type(Notification.class),
      @JsonSubTypes.Type(CategoryNotification.class),
      @JsonSubTypes.Type(DocumentNotification.class),
      @JsonSubTypes.Type(IndexNotification.class),
      @JsonSubTypes.Type(LoginDataNotification.class),
      @JsonSubTypes.Type(PreferenceNotification.class)
})
public class ApiObjectWithJsonType implements Serializable {

    private static final long serialVersionUID = 1L;

    public String getJsonType() {
        return this.getClass().getSimpleName();
    }

    public void setJsonType(String jsonType) {
        // *** do nothing;
    }

}
