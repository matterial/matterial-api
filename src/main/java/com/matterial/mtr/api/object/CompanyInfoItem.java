package com.matterial.mtr.api.object;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>CompanyInfoItem</strong>
 */
@XmlRootElement
public class CompanyInfoItem implements Serializable {

    private static final long serialVersionUID = 1L;

    private String key;
    private String value;

    public CompanyInfoItem() {
        // *** do nothing;
    }

    public CompanyInfoItem(String key) {
        this(key, null);
    }

    public CompanyInfoItem(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
