package com.matterial.mtr.api.object.teams;

import java.io.Serializable;
import java.time.OffsetDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <strong>TeamsEvent</strong>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TeamsEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    public enum EventCategory {
        Notification
    }

    private String tenantId;
    private String tenantName;
    private String tenantUrl;

    private EventCategory eventCategory;
    private String eventType;
    private OffsetDateTime eventDate;
    private String languageKey;
    private String contentStyle;
    private Long instanceId;
    private String instanceDisplayName;
    private String baseUrl;
    private Long personAccountId;
    private String personFirstName;
    private String personLastName;
    private String personAccountLogin;
    private Long documentId;
    private Long documentLanguageVersionId;
    private String documentLanguageVersionLanguageKey;
    private String documentLanguageVersionTitle;
    private Integer documentLanguageVersionVersion;
    private String commentText;
    private Long taskId;
    private String taskDescription;
    private String taskMessage;
    private Boolean taskSnap;
    private String taskStatus;
    private String taskDueDate;
    private String taskAssignee;

    private long receiverAcountId;
    private String receiverFirstName;
    private String receiverLastName;
    private String receiverLogin;
    private String receiverEmail;
    private boolean documentFollowedDirectly;
    private boolean documentFollowedByCategory;
    private boolean documentFollowedByAdditionalProperty;
    private boolean readConfirmationRequested;

    public String getMessageSubject() {
        StringBuilder buffer = new StringBuilder();
        if(this.getEventType().startsWith("DOCUMENT_COMMENT")) {
            buffer.append("New comment for document ");
            buffer.append(this.getDocumentLanguageVersionTitle());
            buffer.append(" by ");
            buffer.append(this.getPersonFirstName());
            buffer.append(" ");
            buffer.append(this.getPersonLastName());
        }
        else if(this.getEventType().startsWith("TASK")) {
            buffer.append("Task changed by ");
            buffer.append(this.getPersonFirstName());
            buffer.append(" ");
            buffer.append(this.getPersonLastName());
            buffer.append(" - ");
            buffer.append(this.getDocumentLanguageVersionTitle());
        }
        else {
            buffer.append(this.getDocumentLanguageVersionTitle());
            buffer.append(" published by ");
            buffer.append(this.getPersonFirstName());
            buffer.append(" ");
            buffer.append(this.getPersonLastName());
        }
        return buffer.toString();
    }

    public void setMessageSubject(String messageSubject) {
        // *** do nothing;
    }

    /*
    public String getMessageBody() {
        StringBuilder buffer = new StringBuilder();
        if(this.getEventType().startsWith("DOCUMENT_COMMENT")) {

            buffer.append("<p>You have received a comment by ");
            buffer.append(this.getPersonFirstName());
            buffer.append(" ");
            buffer.append(this.getPersonLastName());
            buffer.append(" (");
            buffer.append(this.getPersonAccountLogin());
            buffer.append(" ) for a document:</p>" +
                          "<br />" +
                          "<blockquote>" +
                          "  <p>");
            buffer.append(this.getCommentText());
            buffer.append("</p>" +
                          "</blockquote>" +
                          "<p>");
        }
        else if(this.getEventType().startsWith("TASK")) {

            buffer.append("<p>A new ");
            if(this.getTaskSnap() != null && this.getTaskSnap()) {
                buffer.append("flash-");
            }
            buffer.append("task (" +
                          this.getTaskId() +
                          ") was assigned to you by " +
                          this.getPersonFirstName() +
                          " " +
                          this.getPersonLastName() +
                          " (" +
                          this.getPersonAccountLogin() +
                          "): </p>" +
                          "<br />");
            buffer.append("<p>Task: " +
                          this.getTaskDescription() +
                          "</p>" +
                          "<p>Status: " +
                          this.getTaskStatus() +
                          "</p>");
            if(this.getTaskDueDate() != null) {
                buffer.append("<p>Due: " +
                              this.getTaskDueDate() +
                              "</p>");
            }
            if(this.getTaskAssignee() != null) {
                buffer.append("<p>Assignee: " +
                              this.getTaskAssignee() +
                              "</p>");
            }
        }
        else {
            buffer.append("<p>" +
                          this.getPersonFirstName() +
                          " " +
                          this.getPersonLastName() +
                          " (" +
                          this.getPersonAccountLogin() +
                          ") " +
                          "published a document: </p>");
        }

        // *** document link;
        if(this.getDocumentId() != null && this.getLanguageKey() != null) {
            buffer.append("<a href=\"" +
                            this.getBaseUrl()+
                            "#/document/"+
                            this.getDocumentId()+
                            "/" +
                            this.getDocumentLanguageVersionLanguageKey()+
                            "?instanceId=" +
                            this.getInstanceId()+
                            "\"" +
                            "   style=\"border-bottom: 1px solid rgba(0, 45, 60, 0.3);text-decoration: none;color: #66b793;\">" +
                            this.getDocumentLanguageVersionTitle()+
                            " (ID " +
                            this.getDocumentId()+
                            ", Version " +
                            this.getDocumentLanguageVersionVersion()+
                            ")</a>" +
                            "</p>");
        }

        // *** following;
        if(this.getEventType().startsWith("DOCUMENT")) {
            if(this.isDocumentFollowedDirectly() || this.isDocumentFollowedByCategory() || this.isDocumentFollowedByAdditionalProperty()) {
                buffer.append("<p>You are receiving this message because you are following the document:</p>" +
                              "<ul>");
                if(this.isDocumentFollowedDirectly()) {
                    buffer.append("  <li>Directly</li>");
                }
                if(this.isDocumentFollowedByCategory()) {
                    buffer.append("  <li>Via a category</li>");

                }
                if(this.isDocumentFollowedByAdditionalProperty()) {
                    buffer.append("  <li>Via a document property</li>");
                }
                buffer.append("</ul>");
            }

        }

        // *** read confirmation;
        if(this.getEventType().startsWith("DOCUMENT_PUBLISHED")) {
            if(this.isReadConfirmationRequested()) {
                buffer.append("<p>You received this notification because a read confirmation was requested.</p>");
            }
        }

        return buffer.toString();
    }

    public void setMessageBody(String messageBody) {
        // *** do nothing;
    }
    */

    public String getDocumentUrl() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(this.getBaseUrl());
        buffer.append("#/document/");
        buffer.append(this.getDocumentId());
        buffer.append("/");
        buffer.append(this.getDocumentLanguageVersionLanguageKey());
        buffer.append("?instanceId=");
        buffer.append(this.getInstanceId());
        return buffer.toString();
    }

    public void setDocumentUrl(String documentUrl) {
        // *** do nothing;
    }

}
