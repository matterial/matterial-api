package com.matterial.mtr.api.object.meta;

/**
 * <strong>ConvertibleEnhanced</strong>: adds small and large cas-ids to basic Convertible.
 */
public interface ConvertibleEnhanced {

    public String getCasIdImgSmall();
    public String getCasIdImgLarge();

}
