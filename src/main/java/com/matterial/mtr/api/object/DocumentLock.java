package com.matterial.mtr.api.object;

import javax.xml.bind.annotation.XmlRootElement;

import com.matterial.mtr.api.object.meta.Identifiable;

/**
 * <strong>DocumentChangeLog</strong>
 */
@XmlRootElement
public class DocumentLock implements Identifiable {

    private static final long serialVersionUID = 1L;

    private long id;
    private Person person;
    private long documentId;
    private String sessionId;
    private String uniqueLockId;
    private boolean lockedForMe;
    private boolean lockedByMyself;
    private long lockTimestampInSeconds;
    private long previousAccountId;

    public DocumentLock() {
        // *** do nothing;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(long documentId) {
        this.documentId = documentId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUniqueLockId() {
        return uniqueLockId;
    }

    public void setUniqueLockId(String uniqueLockId) {
        this.uniqueLockId = uniqueLockId;
    }

    public boolean isLockedForMe() {
        return lockedForMe;
    }

    public void setLockedForMe(boolean lockedForMe) {
        this.lockedForMe = lockedForMe;
    }

    public boolean isLockedByMyself() {
        return lockedByMyself;
    }

    public void setLockedByMyself(boolean lockedByMyself) {
        this.lockedByMyself = lockedByMyself;
    }

    public long getLockTimestampInSeconds() {
        return lockTimestampInSeconds;
    }

    public void setLockTimestampInSeconds(long lockTimestampInSeconds) {
        this.lockTimestampInSeconds = lockTimestampInSeconds;
    }

    public long getPreviousAccountId() {
        return previousAccountId;
    }

    public void setPreviousAccountId(long previousAccountId) {
        this.previousAccountId = previousAccountId;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(this.getClass().getCanonicalName());
        buffer.append("\nid: ");
        buffer.append(this.id);
        buffer.append("\ndocumentId: ");
        buffer.append(this.documentId);
        buffer.append("\nuniqueLockId: ");
        buffer.append(this.uniqueLockId);
        buffer.append("\nlockedForMe: ");
        buffer.append(this.lockedForMe);
        buffer.append("\nlockedByMyself: ");
        buffer.append(this.lockedByMyself);
        return buffer.toString();
    }

}
