package com.matterial.mtr.api.object;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>DocumentForPdfConversion</strong>
 */
@XmlRootElement
public class DocumentForPdfConversion implements Serializable {

    private static final long serialVersionUID = 1L;

    private Document document;
    private String mainFile;
    private String lastAuthor;
    private String publishingDate;
    private String reviewer;
    private boolean displayHeaderFooter;
    private boolean showLastAuthor;
    private boolean showVersion;
    private boolean showPublishingDate;
    private boolean showReviewer;

    public DocumentForPdfConversion() {
        // *** do nothing;
    }

    public DocumentForPdfConversion(Document document,
                                    String mainFile,
                                    String lastAuthor,
                                    String publishingDate,
                                    String reviewer,
                                    boolean displayHeaderFooter,
                                    boolean showLastAuthor,
                                    boolean showVersion,
                                    boolean showPublishingDate,
                                    boolean showReviewer) {
        this();
        this.document = document;
        this.mainFile = mainFile;
        this.lastAuthor = lastAuthor;
        this.publishingDate = publishingDate;
        this.reviewer = reviewer;
        this.displayHeaderFooter = displayHeaderFooter;
        this.showLastAuthor = showLastAuthor;
        this.showVersion = showVersion;
        this.showPublishingDate = showPublishingDate;
        this.showReviewer = showReviewer;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public String getMainFile() {
        return mainFile;
    }

    public void setMainFile(String mainFile) {
        this.mainFile = mainFile;
    }

    public String getLastAuthor() {
        return lastAuthor;
    }

    public void setLastAuthor(String lastAuthor) {
        this.lastAuthor = lastAuthor;
    }

    public String getPublishingDate() {
        return publishingDate;
    }

    public void setPublishingDate(String publishingDate) {
        this.publishingDate = publishingDate;
    }

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public boolean isDisplayHeaderFooter() {
        return displayHeaderFooter;
    }

    public void setDisplayHeaderFooter(boolean displayHeaderFooter) {
        this.displayHeaderFooter = displayHeaderFooter;
    }

    public boolean isShowLastAuthor() {
        return showLastAuthor;
    }

    public void setShowLastAuthor(boolean showLastAuthor) {
        this.showLastAuthor = showLastAuthor;
    }

    public boolean isShowVersion() {
        return showVersion;
    }

    public void setShowVersion(boolean showVersion) {
        this.showVersion = showVersion;
    }

    public boolean isShowPublishingDate() {
        return showPublishingDate;
    }

    public void setShowPublishingDate(boolean showPublishingDate) {
        this.showPublishingDate = showPublishingDate;
    }

    public boolean isShowReviewer() {
        return showReviewer;
    }

    public void setShowReviewer(boolean showReviewer) {
        this.showReviewer = showReviewer;
    }

}
