package com.matterial.mtr.api.object;

import javax.xml.bind.annotation.XmlRootElement;

import com.matterial.mtr.api.object.meta.ApiObjectWithJsonType;

/**
 * <strong>Notification</strong>
 */
@XmlRootElement
public class Notification extends ApiObjectWithJsonType {

    private static final long serialVersionUID = 1L;

    private String message;
    private String type;

    public Notification() {
        this.type = this.getClass().getSimpleName();
    }

    public Notification(String message) {
        this();
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("jsonType: ");
        buffer.append(this.getJsonType());
        buffer.append(", \nmessage: ");
        buffer.append(this.getMessage());
        buffer.append(", \ntype: ");
        buffer.append(this.getType());
        return buffer.toString();
    }

}
