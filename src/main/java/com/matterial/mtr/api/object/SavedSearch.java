package com.matterial.mtr.api.object;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import com.matterial.mtr.api.object.meta.Identifiable;

/**
 * <strong>SavedSearch</strong>
 */
@XmlRootElement
public class SavedSearch implements Identifiable {

    private static final long serialVersionUID = 1L;

    private long id;
    private String name;
    private boolean personal;
    private int dashboardPrio;
    private int dashboardStyle;
    private List<SavedSearchParameter> params;
    private Set<Long> roleIds;

    public SavedSearch() {
        // *** empty;
    }

    public SavedSearch(long id, String name, boolean personal, int dashboardPrio, int dashboardStyle) {
        this.id = id;
        this.name = name;
        this.personal = personal;
        this.dashboardPrio = dashboardPrio;
        this.dashboardStyle = dashboardStyle;
    }

    /**
     * constructor used to load from db: removes "entity-key-prefix" of all keys;
     */
    public SavedSearch(Number id, String name, Object personal, Number dashboardPrio, Number dashboardStyle) {
        this((id==null)?0L:id.longValue(),
             name,
             // *** personal: oracle: BigDecimal, all other db: Boolean;
             (personal != null && ((personal instanceof Boolean && (Boolean)personal) || (personal instanceof Number && ((Number)personal).intValue() > 0)) ),
             (dashboardPrio==null)?0:dashboardPrio.intValue(),
             (dashboardStyle==null)?0:dashboardStyle.intValue());
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPersonal() {
        return personal;
    }

    public void setPersonal(boolean personal) {
        this.personal = personal;
    }

    public int getDashboardPrio() {
        return dashboardPrio;
    }

    public void setDashboardPrio(int dashboardPrio) {
        this.dashboardPrio = dashboardPrio;
    }

    public int getDashboardStyle() {
        return dashboardStyle;
    }

    public void setDashboardStyle(int dashboardStyle) {
        this.dashboardStyle = dashboardStyle;
    }

    public List<SavedSearchParameter> getParams() {
        if(params == null) {
            this.params = new ArrayList<>();
        }
        return params;
    }

    public void setParams(List<SavedSearchParameter> params) {
        this.params = params;
    }

    public Set<Long> getRoleIds() {
        if(roleIds == null) {
            this.roleIds = new HashSet<>();
        }
        return roleIds;
    }

    public void setRoleIds(Set<Long> roleIds) {
        this.roleIds = roleIds;
    }

}
