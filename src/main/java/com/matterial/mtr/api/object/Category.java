package com.matterial.mtr.api.object;

import java.util.Arrays;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.matterial.mtr.api.object.meta.Identifiable;
import com.matterial.mtr.api.object.meta.IndexableChild;

/**
 * Container representing a category
 */
@XmlRootElement
public class Category extends IndexableChild implements Identifiable, Comparable<Category>  {

    private static final long serialVersionUID = 1L;

    public static final String NAME_QUICK = "Quick";

    public static final String INDEX_FIELD_ID = "id";
    public static final String INDEX_FIELD_NAME = "name";
    public static final String INDEX_FIELD_DESCRIPTION = "description";
    public static final String INDEX_FIELD_QUICK = "quick";
    public static final String INDEX_FIELD_TYPE_NAME = "typeName";
    public static final String INDEX_FIELD_TYPE_ID = "typeId";
    public static final String INDEX_FIELD_TYPE_PERSONAL = "typePersonal";
    public static final String INDEX_FIELD_CAS_ID = "casId";
    public static final String INDEX_FIELD_CAS_ID_THUMBNAIL = "casIdThumbnail";
    public static final String INDEX_FIELD_FILE_SIZE = "fileSize";
    public static final String INDEX_FIELD_FILE_SIZE_THUMBNAIL = "fileSizeThumbnail";
    public static final String INDEX_FIELD_USAGE_COUNT = "usageCount";


    private long id;
    private String name;
    private String description;
    private boolean quick;
    private String typeName;
    private long typeId;
    private boolean typePersonal;
    private String casId;
    private String casIdThumbnail;
    private Long fileSize;
    private Long fileSizeThumbnail;
    private Long usageCount;
    private Boolean following;

    /**
     * Empty Constructor
     */
    public Category() {
        // *** do nothing;
    }

    /**
     * Constructor
     */
    public Category(long id, String name, String description, boolean quick,
                    String typeName, long typeId, boolean typePersonal,
                    String casId, String casIdThumbnail,
                    Long fileSize, Long fileSizeThumbnail,
                    Long usageCount, Boolean following) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.quick = quick;
        this.typeName = typeName;
        this.typeId = typeId;
        this.typePersonal = typePersonal;
        this.casId = casId;
        this.casIdThumbnail = casIdThumbnail;
        this.fileSize = fileSize;
        this.fileSizeThumbnail = fileSizeThumbnail;
        this.usageCount = usageCount;
        this.following = following;
    }

    /**
     * Constructor (used in native query)
     */
    public Category(Number id, String name, String description, Object quick,
                    String typeName, Number typeId, Number typeAccountId,
                    String casId, String casIdThumbnail,
                    Number fileSize, Number fileSizeThumbnail,
                    Number usageCount, Number following) {
        this((id!=null)?id.longValue():0L,
             name,
             description,
             // *** quick: oracle: BigDecimal, all other db: Boolean;
             (quick != null && ((quick instanceof Boolean && (Boolean)quick) || (quick instanceof Number && ((Number)quick).longValue() > 0)) ),
             typeName,
             (typeId!=null)?typeId.longValue():0L,
             (typeAccountId != null && typeAccountId.longValue() > 0L),
             casId,
             casIdThumbnail,
             (fileSize!=null)?fileSize.longValue():null,
             (fileSizeThumbnail!=null)?fileSizeThumbnail.longValue():null,
             (usageCount!=null)?usageCount.longValue():null,
             // *** null => false, 0 => null, > 0 => true;
             (Boolean)((following==null)?false:((following.longValue()>0)?true:(Object)null)) );
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isQuick() {
        return this.quick;
    }

    public void setQuick(boolean quick) {
        this.quick = quick;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public boolean isTypePersonal() {
        return typePersonal;
    }

    public void setTypePersonal(boolean typePersonal) {
        this.typePersonal = typePersonal;
    }

    public String getCasId() {
        return casId;
    }

    public void setCasId(String casId) {
        this.casId = casId;
    }

    public String getCasIdThumbnail() {
        return casIdThumbnail;
    }

    public void setCasIdThumbnail(String casIdThumbnail) {
        this.casIdThumbnail = casIdThumbnail;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Long getFileSizeThumbnail() {
        return fileSizeThumbnail;
    }

    public void setFileSizeThumbnail(Long fileSizeThumbnail) {
        this.fileSizeThumbnail = fileSizeThumbnail;
    }

    public Long getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(Long usageCount) {
        this.usageCount = usageCount;
    }

    public Boolean getFollowing() {
        return following;
    }

    public void setFollowing(Boolean following) {
        this.following = following;
    }

    /**
     * indexing: <br />
     * * INDEX_FIELD_ID, <br />
     * * INDEX_FIELD_NAME, // should be i18n <br />
     * * INDEX_FIELD_TYPE_NAME, // should be i18n <br />
     * * INDEX_FIELD_TYPE_PERSONAL <br />
     * * INDEX_FIELD_TYPE_ID <br />
     */
    @Override
    public Map<String, Object> indexMap() {
        // *** overwritten, to set doNotIndexKeys;
        return this.indexMap(Arrays.asList(INDEX_FIELD_DESCRIPTION,
                                           // *** unnecessary ;
                                           INDEX_FIELD_QUICK,
                                           INDEX_FIELD_CAS_ID,
                                           INDEX_FIELD_CAS_ID_THUMBNAIL,
                                           INDEX_FIELD_FILE_SIZE,
                                           INDEX_FIELD_FILE_SIZE_THUMBNAIL,
                                           INDEX_FIELD_USAGE_COUNT));
    }



    @Override
    public int compareTo(Category o) {
        int result;
        // *** eq;
        if( this.getId() == o.getId() ) {
            result = 0;
        }
        // *** gt;
        else if (this.getId() > o.getId()) {
            result = 1;
        }
        // *** lt;
        else {
            result = -1;
        }
        return result;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Category)) {
            return false;
        }
        Category other = (Category) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }

}
