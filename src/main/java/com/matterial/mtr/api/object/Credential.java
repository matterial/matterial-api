package com.matterial.mtr.api.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>Credential</strong>
 */
@XmlRootElement
public class Credential implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final long SYSTEM_ACCOUNT_ID = 1L;
    public static final String SYSTEM_ACCOUNT_LOGIN = "system";

    private long id;
    private String login;
    private String password;
    private String subscriptionEmail;
    private boolean active;
    private boolean limited;

    private List<DataSource> dataSources;

    private boolean updatePassword;

    public Credential(String login,
                      String password,
                      String subscriptionEmail,
                      boolean active,
                      boolean limited) {
        this(0L, login, password, subscriptionEmail, active, limited);
    }

    public Credential(long id,
                      String login,
                      String subscriptionEmail,
                      boolean active,
                      boolean limited) {
        this(id, login, null, subscriptionEmail, active, limited);
    }

    public Credential(long id,
                      String login,
                      String password,
                      String subscriptionEmail,
                      boolean active,
                      boolean limited) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.subscriptionEmail = subscriptionEmail;
        this.updatePassword = false;
        this.active = active;
        this.limited = limited;
    }

    public Credential() {
        // *** do nothing;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSubscriptionEmail() {
        return subscriptionEmail;
    }

    public void setSubscriptionEmail(String subscriptionEmail) {
        this.subscriptionEmail = subscriptionEmail;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isLimited() {
        return limited;
    }

    public void setLimited(boolean limited) {
        this.limited = limited;
    }

    public List<DataSource> getDataSources() {
        if(this.dataSources == null) {
            this.dataSources = new ArrayList<>();
        }
        return dataSources;
    }

    public void setDataSources(List<DataSource> dataSources) {
        this.dataSources = dataSources;
    }

    public boolean isUpdatePassword() {
        return updatePassword;
    }

    public void setUpdatePassword(boolean updatePassword) {
        this.updatePassword = updatePassword;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("Credential:");
        buffer.append("\nid:");
        buffer.append(this.getId());
        buffer.append("\nlogin:");
        buffer.append(this.getLogin());
        buffer.append("\nactive:");
        buffer.append(this.isActive());
        buffer.append("\nlimited:");
        buffer.append(this.isLimited());
        return buffer.toString();
    }

}
