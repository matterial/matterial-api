package com.matterial.mtr.api.object;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.matterial.mtr.api.object.meta.Identifiable;
import com.matterial.mtr.api.object.meta.IndexableChild;

/**
 * Container representing a comment
 */
@XmlRootElement
public class Comment extends IndexableChild implements Identifiable {

    private static final long serialVersionUID = 1L;

    public static final String INDEX_FIELD_ID = "id";
    public static final String INDEX_FIELD_CREATE_TIME_IN_SECONDS = "createTimeInSeconds";
    public static final String INDEX_FIELD_TEXT = "text";
    public static final String INDEX_FIELD_TEXT_HTML = "textHtml";
    public static final String INDEX_FIELD_DOCUMENT_ID = "documentId";
    public static final String INDEX_FIELD_DOCUMENT_LANGUAGE_VERSION_ID = "documentLanguageVersionId";
    public static final String INDEX_FIELD_DOCUMENT_LANGUAGE_VERSION_VERSION = "documentLanguageVersionVersion";
    public static final String INDEX_FIELD_DOCUMENT_LANGUAGE_VERSION_TITLE = "documentLanguageVersionTitle";
    public static final String INDEX_FIELD_DOCUMENT_LANGUAGE_VERSION_LANGUAGE_KEY = "documentLanguageVersionLanguageKey";
    public static final String INDEX_FIELD_PERSON = "person";
    public static final String INDEX_FIELD_MENTIONED_ACCOUNT_IDS = "mentionedAccountIds";

    private long id;
    private long createTimeInSeconds;
    private String text;
    private String textHtml;

    private long documentId;
    private long documentLanguageVersionId;
    private int documentLanguageVersionVersion;
    private String documentLanguageVersionTitle;
    private String documentLanguageVersionLanguageKey;
    private Person person;

    private List<Long> mentionedAccountIds;

    /**
     * Empty Constructor
     */
    public Comment() {
        // *** do nothing;
    }

    /**
     * Constructor
     */
    public Comment(long id,
                   long createTimeInSeconds,
                   String text,
                   long documentId,
                   long documentLanguageVersionId,
                   int documentLanguageVersionVersion,
                   String documentLanguageVersionTitle,
                   String documentLanguageVersionLanguageKey,
                   Person person) {
        this.id = id;
        this.createTimeInSeconds = createTimeInSeconds;
        this.text = text;
        this.documentId = documentId;
        this.documentLanguageVersionId = documentLanguageVersionId;
        this.documentLanguageVersionVersion = documentLanguageVersionVersion;
        this.documentLanguageVersionTitle = documentLanguageVersionTitle;
        this.documentLanguageVersionLanguageKey = documentLanguageVersionLanguageKey;
        this.person = person;
    }

    /**
     * Constructor (implicit Person construction)
     */
    public Comment(long id,
                   long createTimeInSeconds,
                   String text,
                   long documentId,
                   long documentLanguageVersionId,
                   int documentLanguageVersionVersion,
                   String documentLanguageVersionTitle,
                   String documentLanguageVersionLanguageKey,
                   Long accountId,
                   String accountLogin,
                   Long superiorAccountId,
                   long accountCreateTimeInSeconds,
                   Long accountLastLoginInSeconds,
                   boolean instanceAdmin,
                   boolean demo,
                   boolean limited,
                   boolean active,
                   long contactId,
                   String firstName,
                   String lastName,
                   String position,
                   Long birthdayInSeconds,
                   Integer gender) {
        this(id,
             createTimeInSeconds,
             text,
             documentId,
             documentLanguageVersionId,
             documentLanguageVersionVersion,
             documentLanguageVersionTitle,
             documentLanguageVersionLanguageKey,
             new Person(accountId, accountLogin, superiorAccountId,
                        accountCreateTimeInSeconds, accountLastLoginInSeconds,
                        instanceAdmin, demo, limited, active,
                        contactId, firstName, lastName, position, birthdayInSeconds, gender));
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public long getCreateTimeInSeconds() {
        return createTimeInSeconds;
    }

    public void setCreateTimeInSeconds(long createTimeInSeconds) {
        this.createTimeInSeconds = createTimeInSeconds;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextHtml() {
        return textHtml;
    }

    public void setTextHtml(String textHtml) {
        this.textHtml = textHtml;
    }

    public long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(long documentId) {
        this.documentId = documentId;
    }

    public long getDocumentLanguageVersionId() {
        return documentLanguageVersionId;
    }

    public void setDocumentLanguageVersionId(long documentLanguageVersionId) {
        this.documentLanguageVersionId = documentLanguageVersionId;
    }

    public int getDocumentLanguageVersionVersion() {
        return documentLanguageVersionVersion;
    }

    public void setDocumentLanguageVersionVersion(int documentLanguageVersionVersion) {
        this.documentLanguageVersionVersion = documentLanguageVersionVersion;
    }

    public String getDocumentLanguageVersionTitle() {
        return documentLanguageVersionTitle;
    }

    public void setDocumentLanguageVersionTitle(String documentLanguageVersionTitle) {
        this.documentLanguageVersionTitle = documentLanguageVersionTitle;
    }

    public String getDocumentLanguageVersionLanguageKey() {
        return documentLanguageVersionLanguageKey;
    }

    public void setDocumentLanguageVersionLanguageKey(String documentLanguageVersionLanguageKey) {
        this.documentLanguageVersionLanguageKey = documentLanguageVersionLanguageKey;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<Long> getMentionedAccountIds() {
        if(this.mentionedAccountIds == null) {
            this.mentionedAccountIds = new ArrayList<>();
        }
        return mentionedAccountIds;
    }

    public void setMentionedAccountIds(List<Long> mentionedAccountIds) {
        this.mentionedAccountIds = mentionedAccountIds;
    }

    @Override
    public Language indexLanguage() {
        // ** fallback en;
        Language lang = Language.en;
        try {
            lang = Language.valueOf(this.getDocumentLanguageVersionLanguageKey());
        }
        catch(Exception e) {
            // *** do nothing;
            lang = Language.en;
        }
        return lang;
    }

    @Override
    public Map<String, Object> indexMap() {
        // *** creating index-map;
        // *** only indexing `id`, `text`, and `mentionedAccountIds`;
        Map<String, Object> indexMap = this.indexMap(Arrays.asList(INDEX_FIELD_CREATE_TIME_IN_SECONDS,
                                                                   INDEX_FIELD_TEXT_HTML,
                                                                   INDEX_FIELD_DOCUMENT_ID,
                                                                   INDEX_FIELD_DOCUMENT_LANGUAGE_VERSION_ID,
                                                                   INDEX_FIELD_DOCUMENT_LANGUAGE_VERSION_VERSION,
                                                                   INDEX_FIELD_DOCUMENT_LANGUAGE_VERSION_TITLE,
                                                                   INDEX_FIELD_DOCUMENT_LANGUAGE_VERSION_LANGUAGE_KEY,
                                                                   INDEX_FIELD_PERSON));
        return indexMap;
    }

    @Override
    public void initFromIndexMap(Map<String, Object> indexMap) {
        super.initFromIndexMap(indexMap);
        // *** special handlings;
        // *** mentionedAccountIds;
        Object oMentionedAccountIds = indexMap.get(INDEX_FIELD_MENTIONED_ACCOUNT_IDS);
        if(oMentionedAccountIds != null && oMentionedAccountIds instanceof List) {
            List<?> mentionedAccountIds = (List<?>)oMentionedAccountIds;
            for(Object o : mentionedAccountIds) {
                long mentionedAccountId = 0L;
                if(o != null && o instanceof Number) {
                    mentionedAccountId = ((Number)o).longValue();
                }
                if(mentionedAccountId > 0L) {
                    this.getMentionedAccountIds().add(mentionedAccountId);
                }
            }
        }
    }

}
