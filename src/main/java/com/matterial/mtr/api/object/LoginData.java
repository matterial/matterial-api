package com.matterial.mtr.api.object;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>LoginData</strong>
 */
@XmlRootElement
public class LoginData implements Serializable {

    private static final long serialVersionUID = 1L;

    private String sessionId;
    private long sessionDataTimeout;
    private boolean firstLogin;
    private boolean pwdSetEnabled;
    private boolean pwdExists;
    private boolean pwdDisabled;
    private boolean tfaActive;
    private Licence licence;
    private DataSource currentDataSource;
    private DataSource favouriteDataSource;
    private Person person;
    private Client client;
    private Collection<Language> availableLanguages;
    private List<DataSource> availableDataSources;
    private Map<String, Object> accountSettings;
    private Map<String, Object> clientPreferences;

    /**
     * constructor.
     */
    public LoginData() {
        // *** do nothing;
    }

    public void updateSessionDataTimeout(int sessionDataTimeoutInMinutes) {
        // *** set timeout to 30 minutes later;
        this.sessionDataTimeout = LocalDateTime.now().plusMinutes(sessionDataTimeoutInMinutes).toEpochSecond(ZoneOffset.UTC);
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public long getSessionDataTimeout() {
        return this.sessionDataTimeout;
    }

    public void setSessionDataTimeout(long sessionDataTimeout) {
        this.sessionDataTimeout = sessionDataTimeout;
    }

    public boolean isFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(boolean firstLogin) {
        this.firstLogin = firstLogin;
    }

    public boolean isPwdSetEnabled() {
        return this.pwdSetEnabled;
    }

    public void setPwdSetEnabled(boolean pwdSetEnabled) {
        this.pwdSetEnabled = pwdSetEnabled;
    }

    public boolean isPwdExists() {
        return this.pwdExists;
    }

    public void setPwdExists(boolean pwdExists) {
        this.pwdExists = pwdExists;
    }

    public boolean isPwdDisabled() {
        return this.pwdDisabled;
    }

    public void setPwdDisabled(boolean pwdDisabled) {
        this.pwdDisabled = pwdDisabled;
    }

    public boolean isTfaActive() {
        return tfaActive;
    }

    public void setTfaActive(boolean tfaActive) {
        this.tfaActive = tfaActive;
    }

    public Licence getLicence() {
        return this.licence;
    }

    public void setLicence(Licence licence) {
        this.licence = licence;
        // *** always reset hash;
        if(this.licence != null) {
            this.licence.setHash(null);
        }
    }

    public DataSource getCurrentDataSource() {
        return this.currentDataSource;
    }

    public void setCurrentDataSource(DataSource currentDataSource) {
        this.currentDataSource = currentDataSource;
    }

    public DataSource getFavouriteDataSource() {
        return this.favouriteDataSource;
    }

    public void setFavouriteDataSource(DataSource favouriteDataSource) {
        this.favouriteDataSource = favouriteDataSource;
    }

    public Person getPerson() {
        return this.person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Collection<Language> getAvailableLanguages() {
        if(this.availableLanguages == null) {
            this.availableLanguages = new ArrayList<>();
        }
        return this.availableLanguages;
    }

    public void setAvailableLanguages(List<Language> availableLanguages) {
        this.availableLanguages = availableLanguages;
    }

    public List<DataSource> getAvailableDataSources() {
        if(this.availableDataSources == null) {
            this.availableDataSources = new ArrayList<>();
        }
        return this.availableDataSources;
    }

    public void setAvailableDataSources(List<DataSource> availableDataSources) {
        this.availableDataSources = availableDataSources;
    }

    public Map<String, Object> getAccountSettings() {
        if(this.accountSettings == null) {
            this.accountSettings = new HashMap<>();
        }
        return this.accountSettings;
    }

    public void setAccountSettings(Map<String, Object> accountSettings) {
        this.accountSettings = accountSettings;
    }

    public Map<String, Object> getClientPreferences() {
        if(this.clientPreferences == null) {
            this.clientPreferences = new HashMap<>();
        }
        return this.clientPreferences;
    }

    public void setClientPreferences(Map<String, Object> clientPreferences) {
        this.clientPreferences = clientPreferences;
    }

}
