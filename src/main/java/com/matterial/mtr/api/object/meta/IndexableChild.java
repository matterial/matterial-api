package com.matterial.mtr.api.object.meta;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.matterial.mtr.api.object.ListResultEntry;

/**
 * Interface for classes to be indexable by the EmbeddedSearchServer
 */
public abstract class IndexableChild extends ListResultEntry {

    private static final long serialVersionUID = 1L;

    public enum Language {
        de, en, fr, it, es, sv //, zh
    }

    /**
     * Get language that should be used for index analysis: defaults to null => no language set;
     */
    public Language indexLanguage() {
        return null;
    }

    /**
     * Get a map of all key/values that should be indexed.
     * default implementation uses reflection to fill the map.
     *
     * @return Map of key:String and value:Object
     */
    public Map<String, Object> indexMap() {
        return this.indexMap(null);
    }

    /**
     * Get a map of all key/values that should be indexed.
     * default implementation uses reflection to fill the map.
     *
     * @return Map of key:String and value:Object
     */
    protected Map<String, Object> indexMap(List<String> doNotIndexKeys) {
        Map<String, Object> indexMap = new HashMap<>();
        // *** for each declared method;
        for (Method m : this.getClass().getDeclaredMethods()) {
            if(m != null) {
                String key = null;
                // *** get;
                if(m.getName().startsWith("get")) {
                    key = m.getName().substring(3);
                }
                // *** is;
                else if(m.getName().startsWith("is")) {
                    key = m.getName().substring(2);
                }
                if(key != null) {
                    // *** lowercase the first letter;
                    char c[] = key.toCharArray();
                    c[0] += 32; // c[0] = Character.toLowerCase(c[0]);
                    key = new String(c);
                    if(doNotIndexKeys == null || !doNotIndexKeys.contains(key)) {
                        Object value = null;
                        try {
                            value = m.invoke(this);
                        }
                        catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                            // *** do nothing;
                        }
                        // *** check for null value;
                        if(value != null) {
                            // *** no list or other complex object: add directly to indexMap;
                            if(value.getClass().isPrimitive() ||
                               value instanceof Number ||
                               value instanceof Boolean ||
                               value instanceof String) {
                                indexMap.put(key, value);
                                // *** if language available + string;
                                if(this.indexLanguage() != null &&
                                   value instanceof String) {
                                    // *** handle strings language specific if an indexLanguage is set;
                                    indexMap.put(key+"_"+this.indexLanguage().toString().toLowerCase(), value);
                                }
                            }
                            // *** single IndexableChild;
                            else if(value instanceof IndexableChild) {
                                indexMap.put(key, ((IndexableChild)value).indexMap());
                            }
                            // *** list of IndexableChilds;
                            else if(value instanceof List) {
                                List<Object> listOfObjects = new ArrayList<>();
                                List<Map<String, Object>> listOfIndexableChildren = new ArrayList<>();
                                ((List<?>)value).stream().forEach((entry) -> {
                                    if(entry instanceof IndexableChild) {
                                        listOfIndexableChildren.add(((IndexableChild)entry).indexMap());
                                    }
                                    else {
                                        listOfObjects.add(entry);
                                    }
                                });
                                if(!listOfObjects.isEmpty()) {
                                    indexMap.put(key, listOfObjects);
                                }
                                else if (!listOfIndexableChildren.isEmpty()) {
                                    indexMap.put(key, listOfIndexableChildren);
                                }
                            }
                        }
                    }
                }
            }
        }
        return indexMap;
    }

    /**
     * Init object with data from indexMap.
     * Default implementation uses reflection to fill the object.
     */
    public void initFromIndexMap(Map<String, Object> indexMap) {
        if(indexMap != null) {
            // *** for each entry;
            for(String key : indexMap.keySet()) {
                // *** get value by key;
                Object value = indexMap.get(key);
                // *** only if value not null;
                if(value != null) {
                    // *** no list or other complex object: add directly to object;
                    if(value.getClass().isPrimitive() ||
                       value instanceof Number ||
                       value instanceof Boolean ||
                       value instanceof String) {
                        this._putValueIntoField(this._field(key), value);
                    }
                    // *** single IndexableChild;
                    else if(value instanceof Map) {
                        Map<?, ?> map = (Map<?, ?>)value;
                        this._handleMap(key, map);
                    }
                    // ***  list of IndexableChilds;
                    else if(value instanceof List) {
                        List<?> list = (List<?>)value;
                        this._handleList(key, list);
                    }
                }
            }
        }
    }


    /**
     * helper method.
     */
    private Field _field(String key) {
        Field f = null;
        try {
            f = this.getClass().getDeclaredField(key);
        }
        catch (NoSuchFieldException | SecurityException e) {
            // *** do nothing;
        }
        return f;
    }

    /**
     * helper method.
     */
    private Method _method(String key) {
        // *** lowercase the first letter;
        char c[] = key.toCharArray();
        c[0] -= 32; // c[0] = Character.toUpperCase(c[0]);
        String mName = new String(c);
        mName = "get"+mName;
        Method m = null;
        try {
            m = this.getClass().getDeclaredMethod(mName);
        }
        catch (NoSuchMethodException e) {
            // *** do nothing;
        }
        return m;
    }

    /**
     * helper method.
     */
    @SuppressWarnings("unchecked")
    private void _handleList(String key, List<?> list) {
        Method m = this._method(key);
        if(m != null) {
            try {
                Object obj = m.invoke(this);
                if(obj != null && obj instanceof List) {
                    List<Object> listOfCurrentValues = (List<Object>)obj;
                    // *** foreach map;
                    for (Object o : list) {
                        // *** list-items should be maps;
                        if(o != null && o instanceof Map) {
                            Map<String, Object> map = (Map<String, Object>)o;
                            // *** try to intantiate IndexableChild;
                            Class<?> clazz = IndexableChildEmbeddedListObjectMapping.FIELD_CLASS.get(key);
                            if(clazz != null) {
                                try {
                                    Object instance = clazz.getDeclaredConstructor().newInstance();
                                    if(instance instanceof IndexableChild) {
                                        ((IndexableChild)instance).initFromIndexMap(map);
                                        listOfCurrentValues.add(instance);
                                    }
                                }
                                catch (InstantiationException |
                                       NoSuchMethodException |
                                       SecurityException e) {
                                    // *** do nothing;
                                }
                            }
                        }
                        // *** no map / IndexableChild;
                        /* leads to json exception... Cannot convert Integer to Long;
                        else if(o != null &&
                                (o instanceof Number ||
                                 o instanceof String) ){
                            listOfCurrentValues.add(o);
                        }
                        */
                    }
                }
            }
            catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
                // *** do nothing';
            }
        }
    }

    /**
     * helper method.
     */
    @SuppressWarnings("unchecked")
    private void _handleMap(String key, Map<?, ?> map) {
        Field f = this._field(key);
        if(f != null) {
            try {
                Object obj = f.getType().getDeclaredConstructor().newInstance();
                if(obj instanceof IndexableChild) {
                    // *** init object from map;
                    ((IndexableChild)obj).initFromIndexMap((Map<String, Object>)map);
                    this._putValueIntoField(f, obj);
                }
            }
            catch (InstantiationException |
                   IllegalAccessException |
                   IllegalArgumentException |
                   InvocationTargetException |
                   NoSuchMethodException |
                   SecurityException e) {
                // *** do nothing;
            }
        }
    }

    /**
     * helper method.
     */
    private void _putValueIntoField(Field f, Object value) {
        try {
            if(f != null) {
                // *** access private field;
                f.setAccessible(true);
                try {
                    // *** special handling for Number;
                    if(value instanceof Number) {
                        Class<?> numberType = f.getType();
                        if(numberType != null) {
                            // *** Long;
                            if(numberType.getName().equals(Long.class.getName())) {
                                value = ((Number)value).longValue();
                            }
                            // *** Integer;
                            else if(numberType.getName().equals(Integer.class.getName())) {
                                value = ((Number)value).intValue();
                            }
                            // *** Double;
                            if(numberType.getName().equals(Double.class.getName())) {
                                value = ((Number)value).doubleValue();
                            }
                            // *** Float;
                            if(numberType.getName().equals(Float.class.getName())) {
                                value = ((Number)value).floatValue();
                            }
                        }
                    }
                    f.set(this, value);
                }
                catch (IllegalArgumentException | IllegalAccessException e) {
                    // *** do nothing;
                }
            }
        }
        catch (SecurityException e) {
            // *** do nothing;
        }
    }

}