package com.matterial.mtr.api.object;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>BatchActionStatusNotification</strong>
 */
@XmlRootElement
public class BatchActionStatusNotification extends Notification {

    private static final long serialVersionUID = 1L;

    public static final String MESSAGE = "BATCH_ACTION_STATUS";

    private int actionCode;
    private BatchActionAdditionalData batchActionAdditionalData;
    private List<DocumentClipBoardEntry> successList;
    private List<BatchActionError> errorList;

    public BatchActionStatusNotification() {
        super(MESSAGE);
    }

    public BatchActionStatusNotification(int actionCode, BatchActionAdditionalData batchActionAdditionalData) {
        this();
        this.actionCode = actionCode;
        this.batchActionAdditionalData = batchActionAdditionalData;
    }

    public int getActionCode() {
        return this.actionCode;
    }

    public void setActionCode(int actionCode) {
        this.actionCode = actionCode;
    }

    public BatchActionAdditionalData getBatchActionAdditionalData() {
        return batchActionAdditionalData;
    }

    public void setBatchActionAdditionalData(BatchActionAdditionalData batchActionAdditionalData) {
        this.batchActionAdditionalData = batchActionAdditionalData;
    }

    public boolean isError() {
        return !this.getErrorList().isEmpty();
    }

    public void setError(boolean error) {
        // *** do nothing;
    }

    public List<DocumentClipBoardEntry> getSuccessList() {
        if(this.successList == null) {
            this.successList = new ArrayList<>();
        }
        return successList;
    }

    public void setSuccessList(List<DocumentClipBoardEntry> successList) {
        this.successList = successList;
    }

    public List<BatchActionError> getErrorList() {
        if(this.errorList == null) {
            this.errorList = new ArrayList<>();
        }
        return errorList;
    }

    public void setErrorList(List<BatchActionError> errorList) {
        this.errorList = errorList;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(super.toString());
        buffer.append(", \nerror: ");
        buffer.append(this.isError());
        return buffer.toString();
    }

}
