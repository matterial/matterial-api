package com.matterial.mtr.api.object;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>BatchActionError</strong>
 */
@XmlRootElement
public class BatchActionError implements Serializable {

    private static final long serialVersionUID = 1L;

    private DocumentClipBoardEntry documentClipBoardEntry;
    private int statusCode;

    public BatchActionError() {
        // *** do nothing;
    }

    public BatchActionError(DocumentClipBoardEntry documentClipBoardEntry, int statusCode) {
        this.documentClipBoardEntry = documentClipBoardEntry;
        this.statusCode = statusCode;
    }

    public DocumentClipBoardEntry getDocumentClipBoardEntry() {
        return documentClipBoardEntry;
    }

    public void setDocumentClipBoardEntry(DocumentClipBoardEntry documentClipBoardEntry) {
        this.documentClipBoardEntry = documentClipBoardEntry;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

}
