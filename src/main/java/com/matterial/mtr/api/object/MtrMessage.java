package com.matterial.mtr.api.object;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>MtrMessage</strong>
 */
@XmlRootElement
public class MtrMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String languageKey;
    private long lastModifiedInSeconds;
    private String html;

    /**
     * constructor
     */
    public MtrMessage() {
        // *** do nothing;
    }

    /**
     * constructor
     */
    public MtrMessage(String name, String languageKey, long lastModifiedInSeconds, String html) {
        this.name = name;
        this.languageKey = languageKey;
        this.lastModifiedInSeconds = lastModifiedInSeconds;
        this.html = html;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguageKey() {
        return languageKey;
    }

    public void setLanguageKey(String languageKey) {
        this.languageKey = languageKey;
    }

    public long getLastModifiedInSeconds() {
        return lastModifiedInSeconds;
    }

    public void setLastModifiedInSeconds(long lastModifiedInSeconds) {
        this.lastModifiedInSeconds = lastModifiedInSeconds;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

}
