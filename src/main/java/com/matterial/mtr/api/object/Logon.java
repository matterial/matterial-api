package com.matterial.mtr.api.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>Logon</strong>
 */
@XmlRootElement
public class Logon implements Serializable {

    private static final long serialVersionUID = 1L;

    /** login with password */
    public static final int ORIGIN_INTERNAL_LOGIN = 1;
    /** special origin: task */
    public static final int ORIGIN_INTERNAL_TASK = 2;
    /** special origin: change instance */
    public static final int ORIGIN_CHANGE_INSTANCE = 3;

    /** already authenticated: LTI */
    public static final int ORIGIN_LTI = 4;
    /** already authenticated: Microsoft */
    public static final int ORIGIN_OAUTH_MICROSOFT = 5;
    /** already authenticated: Google */
    public static final int ORIGIN_OAUTH_GOOGLE = 6;
    /** already authenticated: Cobot */
    public static final int ORIGIN_OAUTH_COBOT = 7;

    /** already authenticated: Auto */
    public static final int ORIGIN_AUTO = 8;
    /** already authenticated: Cookie */
    public static final int ORIGIN_COOKIE = 9;
    /** already authenticated: MAGIC_LINK */
    public static final int ORIGIN_MAGIC_LINK = 10;
    /** already authenticated: NTLM */
    public static final int ORIGIN_NTLM = 11;
    /** already authenticated: SAML */
    public static final int ORIGIN_SAML = 12;
    /** already authenticated: Impersonate */
    public static final int ORIGIN_IMPERSONATE = 13;

    /** Login origins, that are allowed to change password without knowing the old one */
    public static final List<Integer> ORIGINS_WITH_PWD_SET_ENABLED;
    static {
        List<Integer> l = new ArrayList<>();
        l.add(ORIGIN_OAUTH_MICROSOFT);
        l.add(ORIGIN_OAUTH_GOOGLE);
        l.add(ORIGIN_OAUTH_COBOT);
        l.add(ORIGIN_MAGIC_LINK);
        ORIGINS_WITH_PWD_SET_ENABLED = Collections.unmodifiableList(l);
    }

    private String login;
    private String password;

    public Logon() {
        // *** do nothing;
    }

    public Logon(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return this.getLogin();
    }

}
