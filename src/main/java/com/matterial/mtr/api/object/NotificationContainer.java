package com.matterial.mtr.api.object;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>NotificationContainer</strong>
 */
@XmlRootElement
public class NotificationContainer implements Serializable {

    private static final long serialVersionUID = 1L;

    private long instanceId;
    private long clientId;
    private Set<Long> accountIds;
    private Notification notification;

    public NotificationContainer() {
        // *** do nothing;
    }

    public NotificationContainer(Notification notification) {
        this(-1L, -1L, null, notification);
    }

    public NotificationContainer(long instanceId, long clientId, Notification notification) {
        this(instanceId, clientId, null, notification);
    }

    public NotificationContainer(long instanceId, long clientId, Set<Long> accountIds, Notification notification) {
        this.instanceId = instanceId;
        this.clientId = clientId;
        this.accountIds = accountIds;
        this.notification = notification;
    }

    public long getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(long instanceId) {
        this.instanceId = instanceId;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public Set<Long> getAccountIds() {
        if(this.accountIds == null) {
            this.accountIds = new HashSet<>();
        }
        return accountIds;
    }

    public void setAccountIds(Set<Long> accountIds) {
        this.accountIds = accountIds;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

}
