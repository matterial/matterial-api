package com.matterial.mtr.api.object.meta;

/**
 * <strong>Convertible</strong>
 */
public interface Convertible {

    public long getId();
    public String getCasId();
    public String getCasIdThumbnail();
    public String getName();

}
