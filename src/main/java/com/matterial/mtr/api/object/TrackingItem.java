package com.matterial.mtr.api.object;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.matterial.mtr.api.object.meta.Identifiable;
import com.matterial.mtr.api.object.meta.Indexable;

/**
 * <strong>TrackingItem</strong>
 */
@XmlRootElement
public class TrackingItem extends Indexable implements Identifiable {

    private static final long serialVersionUID = 1L;

    public static final String INDEX_TYPE_NAME = TrackingItem.class.getSimpleName().toLowerCase();

    public static final String INDEX_FIELD_ID = "id";
    /** accountId - this.loginBean.getAccountId() */
    public static final String INDEX_FIELD_USER_ID = "userId";
    /** objectId - id of handled Identifiable */
    public static final String INDEX_FIELD_OBJECT_ID = "objectId";
    /** creationDate in seconds - Instant.now().getEpochSecond() */
    public static final String INDEX_FIELD_CREATION_DATE_IN_SECONDS = "creationDateInSeconds";
    public static final String INDEX_FIELD_CREATION_DATE = "creationDate";
    /** sessionId - this.loginBean.getSessionId() */
    public static final String INDEX_FIELD_SESSION_ID = "sessionId";
    /** applicationType - always `MTR` */
    public static final String INDEX_FIELD_APP_TYPE = "appType";
    /** applicationInstance - instance + client - this.loginBean.getCurrentDataSourceName()+":"+this.loginBean.getClientId() */
    public static final String INDEX_FIELD_APP_INSTANCE = "appInstance";
    /** actionType - related object */
    public static final String INDEX_FIELD_ACTION_TYPE = "actionType";
    /** actionId - method-name */
    public static final String INDEX_FIELD_ACTION_ID = "actionId";
    /** reference - Handled Type + id OR param-name + id - actionType+": "+bean.getId() / paramName+": "+param */
    public static final String INDEX_FIELD_OBJECT_REF = "objectRef";
    /** context - RestResource ( - "Resource") */
    public static final String INDEX_FIELD_APP_CONTEXT = "appContext";
    /** currently unused */
    public static final String INDEX_FIELD_CHANGE_LOG = "changeLog";

    public static final String ORDER_BY_ID = "id";
    public static final String ORDER_BY_DATE = "date";

    private long id;
    private long userId;
    private long objectId;
    private long creationDateInSeconds;
    private String sessionId;
    private String appType;
    private String appInstance;
    private String actionType;
    private String actionId;
    private String objectRef;
    private String appContext;
    private String changeLog;

    public TrackingItem() {
        // *** do nothing;
    }

    public TrackingItem(
            long id, long userId,
            long objectId, long creationDateInSeconds,
            String sessionId, String appType,
            String appInstance, String actionType,
            String actionId, String objectRef,
            String appContext, String changeLog) {
        this.id = id;
        this.userId = userId;
        this.objectId = objectId;
        this.creationDateInSeconds = creationDateInSeconds;
        this.sessionId = sessionId;
        this.appType = appType;
        this.appInstance = appInstance;
        this.actionType = actionType;
        this.actionId = actionId;
        this.objectRef = objectRef;
        this.appContext = appContext;
        this.changeLog = changeLog;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    public long getCreationDateInSeconds() {
        return creationDateInSeconds;
    }

    public void setCreationDateInSeconds(long creationDateInSeconds) {
        this.creationDateInSeconds = creationDateInSeconds;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getAppInstance() {
        return appInstance;
    }

    public void setAppInstance(String appInstance) {
        this.appInstance = appInstance;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getObjectRef() {
        return objectRef;
    }

    public void setObjectRef(String objectRef) {
        this.objectRef = objectRef;
    }

    public String getAppContext() {
        return appContext;
    }

    public void setAppContext(String appContext) {
        this.appContext = appContext;
    }

    public String getChangeLog() {
        return changeLog;
    }

    public void setChangeLog(String changeLog) {
        this.changeLog = changeLog;
    }

    /**
     * indexTypeName + '-' + id: 'trackingItem-1'.
     */
    @Override
    public String indexId() {
        // *** since elastic-search 6, we have to support single type indices;
        return this.indexTypeName()+"-"+this.getId();
    }

    @Override
    public String autocomplete() {
        return null;
    }

    @Override
    public Map<String, Object> indexMap() {
        // *** creating index-map;
        Map<String, Object> indexMap = super.indexMap();
        // *** indexTypeName to support elasticSearch 6 with single-type indices;
        indexMap.put(INDEX_FIELD_INDEX_TYPE_NAME, this.indexTypeName());
        indexMap.put(INDEX_FIELD_CREATION_DATE, LocalDateTime.ofEpochSecond(this.getCreationDateInSeconds(), 0, ZoneOffset.UTC));
        return indexMap;
    }

}
