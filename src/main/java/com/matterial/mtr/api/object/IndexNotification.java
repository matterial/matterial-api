package com.matterial.mtr.api.object;

import javax.xml.bind.annotation.XmlRootElement;

import com.matterial.mtr.api.object.meta.Indexable;

/**
 * <strong>IndexNotification</strong>
 */
@XmlRootElement
public class IndexNotification extends Notification {

    private static final long serialVersionUID = 1L;

    public static final String ADD = "ADD";
    public static final String REMOVE = "REMOVE";

    private Indexable indexable;

    public IndexNotification() {
        // *** do nothing;
    }

    public IndexNotification(String message,
                             Indexable indexable) {
        super(message);
        this.indexable = indexable;
    }

    public Indexable getIndexable() {
        return indexable;
    }

    public void setIndexable(Indexable indexable) {
        this.indexable = indexable;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(super.toString());
        buffer.append(", \nindexable: ");
        buffer.append(this.getIndexable());
        return buffer.toString();
    }

}
