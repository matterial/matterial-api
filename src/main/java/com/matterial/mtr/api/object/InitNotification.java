package com.matterial.mtr.api.object;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>InitNotification</strong>
 */
@XmlRootElement
public class InitNotification extends Notification {

    private static final long serialVersionUID = 1L;

    public static final String INIT_READY = "INIT_READY";

    public InitNotification() {
        super(INIT_READY);
    }

}
