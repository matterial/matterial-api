package com.matterial.mtr.api.object;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IdTimeValue implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String INDEX_FIELD_ID_TIME_VALUE_ID = "idTimeValueId";
    public static final String INDEX_FIELD_ID_TIME_VALUE_TIME = "idTimeValueTime";


    private Long id;
    private Long timeInSeconds;

    public IdTimeValue() {
        // *** do nothing;
    }

    public IdTimeValue(Long id, Long timeInSeconds) {
        this.id = id;
        this.timeInSeconds = timeInSeconds;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTimeInSeconds() {
        return timeInSeconds;
    }

    public void setTimeInSeconds(Long timeInSeconds) {
        this.timeInSeconds = timeInSeconds;
    }

    /**
     * Return a minimal/light indexMap only
     */
    public Map<String, Object> indexMapLight() {
        Map<String, Object> indexMap = new HashMap<>();
        indexMap.put(INDEX_FIELD_ID_TIME_VALUE_ID, this.getId());
        indexMap.put(INDEX_FIELD_ID_TIME_VALUE_TIME, this.getTimeInSeconds());
        return indexMap;
    }

}
