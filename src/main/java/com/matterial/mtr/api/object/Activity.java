package com.matterial.mtr.api.object;

import com.matterial.mtr.api.object.meta.Identifiable;

/**
 * <strong>Activity</strong>
 */
public class Activity extends ListResultEntry implements Identifiable {

    private static final long serialVersionUID = 1L;

    public static final int NOTIFICATION_STATUS_FALSE = 0;
    public static final int NOTIFICATION_STATUS_PENDING = 1;
    public static final int NOTIFICATION_STATUS_SENT = 2;
    public static final int NOTIFICATION_STATUS_OPENED = 3;

    public static final int NOTIFICATION_EMAIL_OFF = 0;
    public static final int NOTIFICATION_EMAIL_DIRECT = 1;
    public static final int NOTIFICATION_EMAIL_COMBINED = 2;

    private long id;
    private long createTimeInSeconds;
    private String messageType;
    private String languageKey;
    private String contentStyle;
    private Long dataSourceId;
    private String dataSourceDisplayName;
    private Long personAccountId;
    private String personFirstName;
    private String personLastName;
    private String personAccountLogin;
    private Long documentId;
    private Long documentLanguageVersionId;
    private String documentLanguageVersionLanguageKey;
    private String documentLanguageVersionTitle;
    private Integer documentLanguageVersionVersion;
    private String commentText;
    private Long taskId;
    private String taskDescription;
    private String taskMessage;
    private Boolean taskSnap;
    private String taskStatus;
    private String taskDueDate;
    private String taskAssignee;

    private int notificationStatus;
    private long receiverAcountId;
    private String receiverFirstName;
    private String receiverLastName;
    private String receiverLogin;
    private String receiverEmail;
    private boolean documentFollowedDirectly;
    private boolean documentFollowedByCategory;
    private boolean documentFollowedByAdditionalProperty;
    private boolean readConfirmationRequested;

    public Activity() {
        // *** do nothing;
    }

    public Activity(long id,
                    long createTimeInSeconds,
                    String messageType,
                    String languageKey,
                    String contentStyle,
                    Long dataSourceId,
                    String dataSourceDisplayName,
                    Long personAccountId,
                    String personFirstName,
                    String personLastName,
                    String personAccountLogin,
                    Long documentId,
                    Long documentLanguageVersionId,
                    String documentLanguageVersionLanguageKey,
                    String documentLanguageVersionTitle,
                    Integer documentLanguageVersionVersion,
                    String commentText,
                    Long taskId,
                    String taskDescription,
                    String taskMessage,
                    Boolean taskSnap,
                    String taskStatus,
                    String taskDueDate,
                    String taskAssignee,
                    int notificationStatus,
                    long receiverAcountId,
                    String receiverFirstName,
                    String receiverLastName,
                    String receiverLogin,
                    String receiverEmail,
                    boolean documentFollowedDirectly,
                    boolean documentFollowedByCategory,
                    boolean documentFollowedByAdditionalProperty,
                    boolean readConfirmationRequested) {
        this.id = id;
        this.createTimeInSeconds = createTimeInSeconds;
        this.messageType = messageType;
        this.contentStyle = contentStyle;
        this.languageKey = languageKey;
        this.dataSourceId = dataSourceId;
        this.dataSourceDisplayName = dataSourceDisplayName;
        this.personAccountId = personAccountId;
        this.personFirstName = personFirstName;
        this.personLastName = personLastName;
        this.personAccountLogin = personAccountLogin;
        this.documentId = documentId;
        this.documentLanguageVersionId = documentLanguageVersionId;
        this.documentLanguageVersionLanguageKey = documentLanguageVersionLanguageKey;
        this.documentLanguageVersionTitle = documentLanguageVersionTitle;
        this.documentLanguageVersionVersion = documentLanguageVersionVersion;
        this.commentText = commentText;
        this.taskId = taskId;
        this.taskDescription = taskDescription;
        this.taskMessage = taskMessage;
        this.taskSnap = taskSnap;
        this.taskStatus = taskStatus;
        this.taskDueDate = taskDueDate;
        this.taskAssignee = taskAssignee;
        this.notificationStatus = notificationStatus;
        this.receiverAcountId = receiverAcountId;
        this.receiverFirstName = receiverFirstName;
        this.receiverLastName = receiverLastName;
        this.receiverLogin = receiverLogin;
        this.receiverEmail = receiverEmail;
        this.documentFollowedDirectly = documentFollowedDirectly;
        this.documentFollowedByCategory = documentFollowedByCategory;
        this.documentFollowedByAdditionalProperty = documentFollowedByAdditionalProperty;
        this.readConfirmationRequested = readConfirmationRequested;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public long getCreateTimeInSeconds() {
        return createTimeInSeconds;
    }

    public void setCreateTimeInSeconds(long createTimeInSeconds) {
        this.createTimeInSeconds = createTimeInSeconds;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getLanguageKey() {
        return languageKey;
    }

    public void setLanguageKey(String languageKey) {
        this.languageKey = languageKey;
    }

    public String getContentStyle() {
        return contentStyle;
    }

    public void setContentStyle(String contentStyle) {
        this.contentStyle = contentStyle;
    }

    public Long getDataSourceId() {
        return dataSourceId;
    }

    public void setDataSourceId(Long dataSourceId) {
        this.dataSourceId = dataSourceId;
    }

    public String getDataSourceDisplayName() {
        return dataSourceDisplayName;
    }

    public void setDataSourceDisplayName(String dataSourceDisplayName) {
        this.dataSourceDisplayName = dataSourceDisplayName;
    }

    public Long getPersonAccountId() {
        return personAccountId;
    }

    public void setPersonAccountId(Long personAccountId) {
        this.personAccountId = personAccountId;
    }

    public String getPersonFirstName() {
        return personFirstName;
    }

    public void setPersonFirstName(String personFirstName) {
        this.personFirstName = personFirstName;
    }

    public String getPersonLastName() {
        return personLastName;
    }

    public void setPersonLastName(String personLastName) {
        this.personLastName = personLastName;
    }

    public String getPersonAccountLogin() {
        return personAccountLogin;
    }

    public void setPersonAccountLogin(String personAccountLogin) {
        this.personAccountLogin = personAccountLogin;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Long getDocumentLanguageVersionId() {
        return documentLanguageVersionId;
    }

    public void setDocumentLanguageVersionId(Long documentLanguageVersionId) {
        this.documentLanguageVersionId = documentLanguageVersionId;
    }

    public String getDocumentLanguageVersionLanguageKey() {
        return documentLanguageVersionLanguageKey;
    }

    public void setDocumentLanguageVersionLanguageKey(String documentLanguageVersionLanguageKey) {
        this.documentLanguageVersionLanguageKey = documentLanguageVersionLanguageKey;
    }

    public String getDocumentLanguageVersionTitle() {
        return documentLanguageVersionTitle;
    }

    public void setDocumentLanguageVersionTitle(String documentLanguageVersionTitle) {
        this.documentLanguageVersionTitle = documentLanguageVersionTitle;
    }

    public Integer getDocumentLanguageVersionVersion() {
        return documentLanguageVersionVersion;
    }

    public void setDocumentLanguageVersionVersion(Integer documentLanguageVersionVersion) {
        this.documentLanguageVersionVersion = documentLanguageVersionVersion;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTaskMessage() {
        return taskMessage;
    }

    public void setTaskMessage(String taskMessage) {
        this.taskMessage = taskMessage;
    }

    public Boolean getTaskSnap() {
        return taskSnap;
    }

    public void setTaskSnap(Boolean taskSnap) {
        this.taskSnap = taskSnap;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskDueDate() {
        return taskDueDate;
    }

    public void setTaskDueDate(String taskDueDate) {
        this.taskDueDate = taskDueDate;
    }

    public String getTaskAssignee() {
        return taskAssignee;
    }

    public void setTaskAssignee(String taskAssignee) {
        this.taskAssignee = taskAssignee;
    }

    public int getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(int notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public long getReceiverAcountId() {
        return receiverAcountId;
    }

    public void setReceiverAcountId(long receiverAcountId) {
        this.receiverAcountId = receiverAcountId;
    }

    public String getReceiverFirstName() {
        return receiverFirstName;
    }

    public void setReceiverFirstName(String receiverFirstName) {
        this.receiverFirstName = receiverFirstName;
    }

    public String getReceiverLastName() {
        return receiverLastName;
    }

    public void setReceiverLastName(String receiverLastName) {
        this.receiverLastName = receiverLastName;
    }

    public String getReceiverLogin() {
        return receiverLogin;
    }

    public void setReceiverLogin(String receiverLogin) {
        this.receiverLogin = receiverLogin;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public boolean isDocumentFollowedDirectly() {
        return documentFollowedDirectly;
    }

    public void setDocumentFollowedDirectly(boolean documentFollowedDirectly) {
        this.documentFollowedDirectly = documentFollowedDirectly;
    }

    public boolean isDocumentFollowedByCategory() {
        return documentFollowedByCategory;
    }

    public void setDocumentFollowedByCategory(boolean documentFollowedByCategory) {
        this.documentFollowedByCategory = documentFollowedByCategory;
    }

    public boolean isDocumentFollowedByAdditionalProperty() {
        return documentFollowedByAdditionalProperty;
    }

    public void setDocumentFollowedByAdditionalProperty(boolean documentFollowedByAdditionalProperty) {
        this.documentFollowedByAdditionalProperty = documentFollowedByAdditionalProperty;
    }

    public boolean isReadConfirmationRequested() {
        return readConfirmationRequested;
    }

    public void setReadConfirmationRequested(boolean readConfirmationRequested) {
        this.readConfirmationRequested = readConfirmationRequested;
    }

}
