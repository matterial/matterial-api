package com.matterial.mtr.api.object;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.StringTokenizer;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>MtrVersion</strong>
 */
@XmlRootElement
public class MtrVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    private String moduleName;
    private int major;
    private int minor;
    private int patch;
    private String info;
    private Long timeStampInMillis;
    private String timestamp;
    private String scmBranch;
    private String commit;
    private String buildNumber;

    public MtrVersion() {
        // *** do nothing;
    }

    public MtrVersion(String moduleName,
                      String version,
                      Long timeStampInMillis,
                      String timestamp,
                      String scmBranch,
                      String commit,
                      String buildNumber) {
        int major = 0;
        int minor = 0;
        int patch = 0;
        String info = null;
        if(version != null && !version.trim().isEmpty()) {
            StringTokenizer st = new StringTokenizer(version.trim(), ".-");
            int i=0;
            while (st.hasMoreTokens()) {
                String token = st.nextToken();
                try {
                    if(i==0) {
                        major = Integer.valueOf(token);
                    }
                    else if(i==1) {
                        minor = Integer.valueOf(token);
                    }
                    else if(i==2) {
                        patch = Integer.valueOf(token);
                    }
                    else if(i==3) {
                        info = token;
                    }
                    else {
                        break;
                    }
                }
                catch(NumberFormatException e) {
                    // *** do nothing;
                }
                i++;
            }
        }
        this.moduleName = moduleName;
        this.major = major;
        this.minor = minor;
        this.patch = patch;
        this.info = info;
        this.timeStampInMillis = timeStampInMillis;
        this.timestamp = timestamp;
        this.scmBranch = scmBranch;
        this.commit = commit;
        this.buildNumber = buildNumber;
    }

    public MtrVersion(String moduleName,
                      int major,
                      int minor,
                      int patch,
                      String info,
                      Long timeStampInMillis,
                      String timestamp,
                      String scmBranch,
                      String commit,
                      String buildNumber) {
        this.moduleName = moduleName;
        this.major = major;
        this.minor = minor;
        this.patch = patch;
        this.info = info;
        this.timeStampInMillis = timeStampInMillis;
        this.timestamp = timestamp;
        this.scmBranch = scmBranch;
        this.commit = commit;
        this.buildNumber = buildNumber;
    }

    public String getModuleName() {
        return this.moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }

    public int getPatch() {
        return patch;
    }

    public void setPatch(int patch) {
        this.patch = patch;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Long getTimeStampInMillis() {
        return timeStampInMillis;
    }

    public void setTimeStampInMillis(Long timeStampInMillis) {
        this.timeStampInMillis = timeStampInMillis;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getScmBranch() {
        return scmBranch;
    }

    public void setScmBranch(String scmBranch) {
        this.scmBranch = scmBranch;
    }

    public String getCommit() {
        return commit;
    }

    public void setCommit(String commit) {
        this.commit = commit;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(this.getModuleName());
        buffer.append(": ");
        buffer.append(this.getMajor());
        buffer.append(".");
        buffer.append(this.getMinor());
        buffer.append(".");
        buffer.append(this.getPatch());
        if(this.getInfo() != null && !this.getInfo().trim().isEmpty()) {
            buffer.append("-");
            buffer.append(this.getInfo());
        }
        buffer.append(" build #");
        buffer.append(this.getBuildNumber());
        buffer.append(" (");
        buffer.append(this.getTimestamp());
        buffer.append(", branch ");
        buffer.append(this.getScmBranch());
        buffer.append(", commit ");
        buffer.append(this.getCommit());
        buffer.append(")");
        return buffer.toString();
    }

    /**
     * test-main
     */
    public static void main(String[] args) {
        MtrVersion v = new MtrVersion("matterial MTR",
                                      "1.2.3-dev",
                                      LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)*1000L,
                                      LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME),
                                      "master",
                                      "fsklf32",
                                      "0815");
        System.out.println(v);
    }

}
