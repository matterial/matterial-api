package com.matterial.mtr.api.object;

import javax.xml.bind.annotation.XmlRootElement;

import com.matterial.mtr.api.object.meta.ApiObjectWithJsonType;

/**
 * <strong>ListResultEntry</strong>
 */
@XmlRootElement
public class ListResultEntry extends ApiObjectWithJsonType {

    private static final long serialVersionUID = 1L;

}
