package com.matterial.mtr.api.object;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>ClipboardNotification</strong>
 */
@XmlRootElement
public class ClipboardNotification extends Notification {

    private static final long serialVersionUID = 1L;

    public static final String CLIPBOARD_CHANGE = "CLIPBOARD_CHANGE";

    public ClipboardNotification() {
        super(CLIPBOARD_CHANGE);
    }

}
