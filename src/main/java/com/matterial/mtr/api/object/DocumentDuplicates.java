package com.matterial.mtr.api.object;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>DocumentDuplicates</strong> contains a map of all duplicated documents.
 */
@XmlRootElement
public class DocumentDuplicates implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * key: contextToken;
     * value: DocumentDuplicate, which represents a duplicated multi-language-document;
     */
    private Map<String, DocumentDuplicate> map;

    public DocumentDuplicates() {
        // *** do nothing;
    }

    public Map<String, DocumentDuplicate> getMap() {
        if(this.map == null) {
            this.map = new HashMap<>();
        }
        return map;
    }


    public void setMap(Map<String, DocumentDuplicate> map) {
        this.map = map;
    }

}
