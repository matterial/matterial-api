package com.matterial.mtr.api.object;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>Invitee</strong>: Credential with additional information, if invitation-link has expired.
 */
@XmlRootElement
public class Invitee extends Credential {

    private static final long serialVersionUID = 1L;

    private boolean invitationLinkExpired;

    public Invitee() {
        // *** do nothing;
    }

    public Invitee(long id,
                   String login,
                   String subscriptionEmail,
                   boolean active,
                   boolean limited,
                   Long tokenValidEndInSeconds) {
        super(id, login, subscriptionEmail, active, limited);
        this.invitationLinkExpired = (tokenValidEndInSeconds != null &&
                                      tokenValidEndInSeconds < LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
    }

    public boolean isInvitationLinkExpired() {
        return invitationLinkExpired;
    }

    public void setInvitationLinkExpired(boolean invitationLinkExpired) {
        this.invitationLinkExpired = invitationLinkExpired;
    }

}
