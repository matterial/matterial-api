package com.matterial.mtr.api.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>BatchActionAdditionalData</strong>
 */
@XmlRootElement
public class BatchActionAdditionalData implements Serializable {

    private static final long serialVersionUID = 1L;

    // *** additional data for action ARCHIVE;
    private Long archivedBeginInSecondsRequest;
    // *** additional data for action SET/ADD/REMOVE/_CATEGORIES;
    private Set<Long> categoryIds;
    // *** additional data for action SET/ADD/REMOVE/_ADDITIONAL_PROPERTIES;
    private List<AdditionalProperty> additionalProperties;
    // *** additional data for action SET_RESPONSIBLES;
    private Set<Long> contactIds;
    // *** additional data for action SET_/ADD_/REMOVE/_ROLE_RIGHTS;
    private List<RoleRight> roleRights;

    public BatchActionAdditionalData() {
        // *** do nothing;
    }

    public Long getArchivedBeginInSecondsRequest() {
        return archivedBeginInSecondsRequest;
    }

    public void setArchivedBeginInSecondsRequest(Long archivedBeginInSecondsRequest) {
        this.archivedBeginInSecondsRequest = archivedBeginInSecondsRequest;
    }

    public Set<Long> getCategoryIds() {
        if(this.categoryIds == null) {
            this.categoryIds = new HashSet<>();
        }
        return categoryIds;
    }

    public void setCategoryIds(Set<Long> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<AdditionalProperty> getAdditionalProperties() {
        if(this.additionalProperties == null) {
            this.additionalProperties = new ArrayList<>();
        }
        return additionalProperties;
    }

    public void setAdditionalProperties(List<AdditionalProperty> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public Set<Long> getContactIds() {
        if(this.contactIds == null) {
            this.contactIds = new HashSet<>();
        }
        return contactIds;
    }

    public void setContactIds(Set<Long> contactIds) {
        this.contactIds = contactIds;
    }

    public List<RoleRight> getRoleRights() {
        if(this.roleRights == null) {
            this.roleRights = new ArrayList<>();
        }
        return roleRights;
    }

    public void setRoleRights(List<RoleRight> roleRights) {
        this.roleRights = roleRights;
    }

}
