package com.matterial.mtr.api.object.meta;

/**
 * <strong>ConvertiblePdf</strong>: adds pdf-cas-id to basic Convertible.
 */
public interface ConvertiblePdf {

    public String getCasIdPdf();

}
