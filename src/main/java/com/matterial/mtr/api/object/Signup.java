package com.matterial.mtr.api.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * <strong>Signup</strong>
 */
@XmlRootElement
public class Signup implements Serializable {

    private static final long serialVersionUID = 1L;

    public enum InstanceType {
        DEFAULT,
        COOP
    }

    private String subscriptionEmail;
    private String password;
    private String dataSourceDisplayName;
    private String dataSourceReference;
    private Integer dataSourceActivationDays;
    private List<Long> activeLanguageIds;
    private boolean updatePassword;
    private Licence licence;
    private List<CompanyInfoItem> companyInfoItems;
    private boolean coop;

    public Signup() {
        // *** do nothing;
    }

    public String getSubscriptionEmail() {
        return subscriptionEmail;
    }

    public void setSubscriptionEmail(String subscriptionEmail) {
        this.subscriptionEmail = subscriptionEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDataSourceDisplayName() {
        return dataSourceDisplayName;
    }

    public void setDataSourceDisplayName(String dataSourceDisplayName) {
        this.dataSourceDisplayName = dataSourceDisplayName;
    }

    public String getDataSourceReference() {
        return dataSourceReference;
    }

    public void setDataSourceReference(String dataSourceReference) {
        this.dataSourceReference = dataSourceReference;
    }

    public Integer getDataSourceActivationDays() {
        return dataSourceActivationDays;
    }

    public void setDataSourceActivationDays(Integer dataSourceActivationDays) {
        this.dataSourceActivationDays = dataSourceActivationDays;
    }

    public List<Long> getActiveLanguageIds() {
        if(this.activeLanguageIds == null) {
            this.activeLanguageIds = new ArrayList<>();
        }
        return activeLanguageIds;
    }

    public void setActiveLanguageIds(List<Long> activeLanguageIds) {
        this.activeLanguageIds = activeLanguageIds;
    }

    public boolean isUpdatePassword() {
        return updatePassword;
    }

    public void setUpdatePassword(boolean updatePassword) {
        this.updatePassword = updatePassword;
    }

    public Licence getLicence() {
        return licence;
    }

    public void setLicence(Licence licence) {
        this.licence = licence;
    }

    public List<CompanyInfoItem> getCompanyInfoItems() {
        if(this.companyInfoItems == null) {
            this.companyInfoItems = new ArrayList<>();
        }
        return companyInfoItems;
    }

    public void setCompanyInfoItems(List<CompanyInfoItem> companyInfoItems) {
        this.companyInfoItems = companyInfoItems;
    }

    public boolean isCoop() {
        return coop;
    }

    public void setCoop(boolean coop) {
        this.coop = coop;
    }

    /**
     * get requested instance type.
     */
    public InstanceType instanceType() {
        if(this.coop) {
            return InstanceType.COOP;
        }
        return InstanceType.DEFAULT;
    }

}
