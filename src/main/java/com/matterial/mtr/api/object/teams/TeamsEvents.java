package com.matterial.mtr.api.object.teams;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <strong>TeamsEvents</strong>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TeamsEvents {

    private List<TeamsEvent> events;

}
