package com.matterial.mtr.api;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * <strong>StatusCode</strong>
 */
public class StatusCode {

    private static final Map<Integer, String> DESCRIPTION_MAP;
    static {
        Map<Integer, String> map = new HashMap<>();
        // *** foreach constant within StatusCode;
        for (Field field : StatusCode.class.getFields()) {
            try {
                // *** get value of const;
                Object fieldValue = field.get(null);
                if(fieldValue != null && fieldValue instanceof Integer) {
                    // *** add value + fieldName to map;
                    map.put(((Integer)fieldValue), field.getName());
                }
            }
            catch (Exception e) {
                // *** do nothing;
            }
        }
        DESCRIPTION_MAP = Collections.unmodifiableMap(map);
    }

    public static String getDescription(int statusCode) {
        return DESCRIPTION_MAP.get(statusCode);
    }

    public static final String HEADER_STATUS_CODE = "mtrStatusCode";
    public static final String HEADER_STATUS_MSG = "mtrStatusMsg";

    // *** general status starting from 0;
    public static final int GENERAL_OK = 0;
    public static final int GENERAL_UNKNOWN = 1;
    public static final int GENERAL_INVALID_IDS = 2;
    public static final int ADDRESS_INVALID = 3;
    public static final int COMMUNICATION_DATA_TYPE_INVALID = 4;
    public static final int COMMUNICATION_DATA_INVALID = 5;
    public static final int LOGON_NOT_LOGGED_IN = 6;
    public static final int LOGON_CREDENTIALS_INVALID = 7;
    public static final int I18N_INVALID = 8;
    public static final int PASSWORD_INVALID = 9;
    public static final int DATASOURCE_INVALID = 10;
    public static final int SYSTEM_ACCOUNT_REQUIRED = 11;
    public static final int CREDENTIAL_INVALID = 12;
    public static final int PREFERENCE_INVALID = 13;
    public static final int GENERAL_INVALID_API_OBJECT = 14;
    public static final int BAD_REQUEST = 15;
    public static final int GENERAL_CONFLICT = 16;
    public static final int GENERAL_NOT_EXIST = 17;
    public static final int FORBIDDEN = 18;
    public static final int LICENCE_INVALID = 19;
    public static final int LICENCE_USER_LIMIT = 20;
    public static final int LICENCE_CAS_SIZE_LIMIT = 21;
    public static final int INSTANCE_OWNER_REQUIRED = 22;
    public static final int LICENCE_REVIEW_GROUP_REQUIRED = 25;
    public static final int LOGON_NO_VALID_DATA_SOURCE = 27;
    public static final int REMOTE_ADDR_NOT_ALLOWED = 28;
    public static final int TABLE_LOCK_INVALID = 29;
    public static final int LOGON_INVALID_SERVER = 30;
    public static final int OAUTH2_NO_ACCESS_TOKEN = 31;
    public static final int OAUTH2_NOT_SUPPORTED = 32;
    public static final int OAUTH2_NO_USER_INFO = 33;
    public static final int AUTH_2FA_NOT_SUPPORTED = 34;
    public static final int AUTH_2FA_LOGIN_INVALID = 35;
    public static final int AUTH_2FA_KEY_INVALID = 36;
    public static final int AUTH_2FA_VERFICATION_CODE_INVALID = 37;
    public static final int AUTH_2FA_REQUIRED = 38;
    public static final int LOGIN_TOKEN_INVALID = 39;
    public static final int CREDENTIAL_NOT_INVITABLE_SYSTEM = 40;
    public static final int CREDENTIAL_NOT_INVITABLE_CURRENT = 41;
    public static final int CREDENTIAL_NOT_INVITABLE_EXISTING = 42;
    public static final int LICENCE_USER_LIMITED_LIMIT = 43;
    public static final int OAUTH2_NO_STATE = 44;
    public static final int OAUTH2_UNSUPPORTED_STATE = 45;
    public static final int NOT_FOUND = 46;
    public static final int LOGIN_FORBIDDEN_SSO_ONLY = 47;

    // *** document status starting from 100;
    public static final int DOCUMENT_INVALID = 101;
    public static final int DOCUMENT_LANGUAGE_VERSION_INVALID = 102;
    public static final int ATTACHMENT_INVALID = 103;
    public static final int DOCUMENT_FORBIDDEN = 104;
    public static final int ADDITIONAL_PROPERTY_NOT_FOUND = 105;
    public static final int SAVED_SEARCH_INVALID = 106;
    public static final int ADDITIONAL_PROPERTY_INVALID = 107;
    public static final int EXTENSION_VALUE_INVALID = 108;
    public static final int DOCUMENT_LOCK_INVALID = 109;
    public static final int DOCUMENT_STATUS = 110;
    public static final int DOCUMENT_LANGUAGE_COUNT = 111;
    public static final int DOCUMENT_TITLE_EXCEEDS_LENGTH = 112;
    public static final int DOCUMENT_ABSTRACT_EXCEEDS_LENGTH = 113;
    public static final int DOCUMENT_VERSION_COMMENT_EXCEEDS_LENGTH = 114;
    public static final int DOCUMENT_CLIP_BOARD_EXCEEDS_LIMIT = 115;
    public static final int DOCUMENT_NOT_IN_TRASH = 116;
    public static final int DOCUMENT_CLIP_BOARD_NOTHING_UPDATED = 117;
    public static final int DOCUMENT_TRANSLATION_DEEPL_BAD_REQUEST = 118;
    public static final int DOCUMENT_TRANSLATION_DEEPL_AUTHORIZATION_FAILED = 119;
    public static final int DOCUMENT_TRANSLATION_DEEPL_NOT_FOUND = 120;
    public static final int DOCUMENT_TRANSLATION_DEEPL_REQUEST_SIZE_EXCEEDS_LIMIT = 121;
    public static final int DOCUMENT_TRANSLATION_DEEPL_TOO_MANY_REQUESTS = 122;
    public static final int DOCUMENT_TRANSLATION_DEEPL_QUOTA_EXCEEDED = 123;
    public static final int DOCUMENT_TRANSLATION_DEEPL_RESOURCE_CURRENTLY_UNAVAILABLE = 124;
    public static final int DOCUMENT_TRANSLATION_DEEPL_INTERNAL_ERROR = 125;
    public static final int DOCUMENT_CUSTOM_VERSION_STRING_EXCEEDS_LENGTH = 126;
    public static final int DOCUMENT_PUBLISH_WITHOUT_CATEGORIES_NOT_ALLOWED = 127;
    public static final int DOCUMENT_PUBLISH_UNREVIEWED_NOT_ALLOWED = 128;
    public static final int DOCUMENT_PUBLISH_IMMEDIATELY_REVIEWED_NOT_ALLOWED = 129;
    public static final int DOCUMENT_PUBLISH_WITHOUT_CUSTOM_VERSION_NOT_ALLOWED = 130;
    public static final int DOCUMENT_PUBLISH_ONLY_ALLOWED_FROM_DRAFT = 131;
    public static final int DOCUMENT_PUBLISH_WITHOUT_VERSION_COMMENT_NOT_ALLOWED = 132;

    // ### category/categoryType status codes
    public static final int CATEGORY_TYPE_IN_USE = 200;
    public static final int CATEGORY_TYPE_NOT_FOUND = 201;
    public static final int CATEGORY_NOT_FOUND = 202;
    public static final int CATEGORY_TYPE_INVALID = 203;
    public static final int CATEGORY_INVALID = 204;

    // *** role status starting from 300;
    public static final int ROLE_INVALID = 301;

    // *** cas status starting from 400;
    public static final int CAS_INVALID = 401;

    // ### task status codes
    public static final int TASK_NOT_FOUND = 500;
    public static final int TASK_STATUS_NOT_FOUND = 501;
    public static final int TASK_INVALID = 502;
    public static final int TASK_STATUS_INVALID = 503;
    public static final int TASK_INVALID_DESCRIPTION = 504;
    public static final int TASK_INVALID_ASSIGNED_ROLE = 505;
    public static final int TASK_INVALID_STATUS = 506;
    public static final int TASK_ACCOUNT_ASSIGNED = 507;
    public static final int TASK_NO_ACCOUNT_ASSIGNED = 508;
    public static final int TASK_WRONG_ACCOUNT_ASSIGNED = 509;
    public static final int TASK_NO_RESUBMISSION_PERIOD = 510;
    public static final int TASK_INVALID_RESUBMISSION_PERIOD = 511;
    public static final int TASK_AUTHOR_INVALID = 512;
    public static final int TASK_INVALID_RESUBMISSION_TIME_UNIT = 513;
    public static final int TASK_NO_RESUBMISSION_TIME_UNIT = 514;
    public static final int TASK_INVALID_MESSAGE = 515;

    // ### person status codes
    public static final int PERSON_NOT_FOUND = 600;
    public static final int PERSON_ACCOUNT_LOGIN_ALREADY_EXISTS = 601;
    public static final int PERSON_INVALID = 602;
    public static final int PERSON_DUPLICATE_NAME = 603;
    public static final int ACCOUNT_SETTING_INVALID = 604;
    public static final int CONTACT_IMAGE_INVALID = 605;
    public static final int CONTACT_FIRST_NAME_EXCEEDS_LENGTH = 606;
    public static final int CONTACT_LAST_NAME_EXCEEDS_LENGTH = 607;
    public static final int CONTACT_POSITION_EXCEEDS_LENGTH = 608;
    public static final int ACCOUNT_LOGIN_EXCEEDS_LENGTH = 609;
    public static final int ADDRESS_POSTAL_CODE_EXCEEDS_LENGTH = 610;
    public static final int ADDRESS_CITY_EXCEEDS_LENGTH = 611;
    public static final int ADDRESS_COUNTRY_EXCEEDS_LENGTH = 612;
    public static final int ADDRESS_STREET_EXCEEDS_LENGTH = 613;
    public static final int ADDRESS_HOUSE_NUMBER_EXCEEDS_LENGTH = 614;
    public static final int COMMUNICATION_DATA_VALUE_EXCEEDS_LENGTH = 615;
    public static final int COMMUNICATION_DATA_DESCRIPTION_EXCEEDS_LENGTH = 616;

    // *** client status starting from 700;
    public static final int CLIENT_INVALID = 701;
    public static final int CLIENT_PREFERENCE_INVALID = 702;

    // *** language status starting from 800;
    public static final int LANGUAGE_INVALID = 801;

    // ### file related status codes
    public static final int FILE_INVALID = 900;
    public static final int FILE_SIZE_LARGE = 901;

    // *** notification related status codes;
    public static final int NOTIFICATION_ERROR = 1001;
    public static final int NOTIFICATION_NOT_SUPPORTED = 1002;
    public static final int NOTIFICATION_NOT_ENABLED = 1003;
    public static final int NOTIFICATION_MESSAGING_SERVICE_ERROR = 1004;
    public static final int NOTIFICATION_REMOTE_UPDATE_ERROR = 1005;
    public static final int NOTIFICATION_DELAYED = 1006;
    public static final int NOTIFICATION_TEAMS_SERVICE_ERROR = 1007;

    // *** activity related status codes;
    public static final int ACTIVITY_INVALID = 1101;

    // *** comment status codes
    public static final int COMMENT_INVALID = 1201;
    public static final int COMMENT_INVALID_TEXT = 1202;
    public static final int COMMENT_INVALID_DLV = 1203;

    // *** company info status codes;
    public static final int COMPANY_INFO_INVALID = 1301;

    // *** tracking status starting from 10000;
    public static final int TRACKING_ITEM_INVALID = 10001;

}
