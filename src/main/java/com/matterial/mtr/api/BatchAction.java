package com.matterial.mtr.api;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.matterial.mtr.api.object.BatchActionAdditionalData;

/**
 * <strong>BatchAction</strong>
 */
public class BatchAction {

    public final static int TRASH = 1;
    public final static int UNTRASH = 2;
    public final static int ARCHIVE = 3;
    public final static int UNARCHIVE = 4;
    public final static int FINALLY_REMOVE = 5;
    public final static int SET_CATEGORIES = 6;
    public final static int ADD_CATEGORIES = 7;
    public final static int REMOVE_CATEGORIES = 8;
    public final static int SET_ADDITIONAL_PROPERTIES = 9;
    public final static int ADD_ADDITIONAL_PROPERTIES = 10;
    public final static int REMOVE_ADDITIONAL_PROPERTIES = 11;
    public final static int CREATE_BOOKLET = 12;
    public final static int SET_RESPONSIBLES = 13;
    public final static int SET_ROLE_RIGHTS = 14;
    public final static int ADD_ROLE_RIGHTS = 15;
    public final static int REMOVE_ROLE_RIGHTS = 16;

    private static final Map<Integer, String> MAP;
    static {
        Map<Integer, String> m = new HashMap<>();
        // *** foreach constant within BatchAction;
        for (Field actionCodeField : BatchAction.class.getFields()) {
            try {
                // *** BatchAction.CONST => FIELDVALUE;
                Object o = actionCodeField.get(null);
                if(o != null && o instanceof Integer) {
                    Integer actionCode = (Integer)o;
                    m.put(actionCode, actionCodeField.getName());
                }
            }
            catch (Exception e) {
                // *** do nothing;
            }
        }
        MAP = Collections.unmodifiableMap(m);
    }

    private int actionCode;
    private String name;
    private BatchActionAdditionalData batchActionAdditionalData;

    public BatchAction(int actionCode, BatchActionAdditionalData batchActionAdditionalData) {
        this.actionCode = actionCode;
        this.name = MAP.get(actionCode);
        if(this.name == null) {
            this.name = "~~UNSUPPORTED ACTION~~";
        }
        this.batchActionAdditionalData = batchActionAdditionalData;
    }

    public int getActionCode() {
        return this.actionCode;
    }

    public BatchActionAdditionalData getBatchActionAdditionalData() {
        return batchActionAdditionalData;
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(this.name);
        buffer.append("(");
        buffer.append(this.actionCode);
        buffer.append(")");
        return buffer.toString();
    }

    public boolean equals(int actionCode) {
        return this.actionCode == actionCode;
    }

    /**
     * test main
     */
    public static void main(String[] args) {
        System.out.println(new BatchAction(TRASH, null));
        System.out.println(new BatchAction(UNTRASH, null));
        System.out.println(new BatchAction(ARCHIVE, null));
        System.out.println(new BatchAction(UNARCHIVE, null));
        System.out.println(new BatchAction(FINALLY_REMOVE, null));
        System.out.println(new BatchAction(SET_CATEGORIES, null));
        System.out.println(new BatchAction(ADD_CATEGORIES, null));
        System.out.println(new BatchAction(REMOVE_CATEGORIES, null));
        System.out.println(new BatchAction(SET_ADDITIONAL_PROPERTIES, null));
        System.out.println(new BatchAction(ADD_ADDITIONAL_PROPERTIES, null));
        System.out.println(new BatchAction(REMOVE_ADDITIONAL_PROPERTIES, null));
        System.out.println(new BatchAction(CREATE_BOOKLET, null));
        System.out.println(new BatchAction(SET_RESPONSIBLES, null));
        System.out.println(new BatchAction(SET_ROLE_RIGHTS, null));
        System.out.println(new BatchAction(ADD_ROLE_RIGHTS, null));
        System.out.println(new BatchAction(REMOVE_ROLE_RIGHTS, null));
        System.out.println(new BatchAction(99, null));
    }

}
