package com.matterial.mtr.api;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import javax.xml.bind.JAXB;

import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.victools.jsonschema.generator.Option;
import com.github.victools.jsonschema.generator.OptionPreset;
import com.github.victools.jsonschema.generator.SchemaGenerator;
import com.github.victools.jsonschema.generator.SchemaGeneratorConfig;
import com.github.victools.jsonschema.generator.SchemaGeneratorConfigBuilder;
import com.github.victools.jsonschema.generator.SchemaVersion;
import com.matterial.mtr.api.object.AccountSetting;
import com.matterial.mtr.api.object.Activity;
import com.matterial.mtr.api.object.AdditionalProperty;
import com.matterial.mtr.api.object.Address;
import com.matterial.mtr.api.object.Attachment;
import com.matterial.mtr.api.object.BatchActionAdditionalData;
import com.matterial.mtr.api.object.BatchActionError;
import com.matterial.mtr.api.object.BatchActionStatusNotification;
import com.matterial.mtr.api.object.Category;
import com.matterial.mtr.api.object.CategoryNotification;
import com.matterial.mtr.api.object.CategoryType;
import com.matterial.mtr.api.object.Client;
import com.matterial.mtr.api.object.ClipboardNotification;
import com.matterial.mtr.api.object.Comment;
import com.matterial.mtr.api.object.CommunicationData;
import com.matterial.mtr.api.object.CompanyInfoItem;
import com.matterial.mtr.api.object.ContactImage;
import com.matterial.mtr.api.object.Credential;
import com.matterial.mtr.api.object.CredentialWithInvitationText;
import com.matterial.mtr.api.object.DataSource;
import com.matterial.mtr.api.object.Document;
import com.matterial.mtr.api.object.DocumentChangeLog;
import com.matterial.mtr.api.object.DocumentChangeLogMap;
import com.matterial.mtr.api.object.DocumentClipBoardEntry;
import com.matterial.mtr.api.object.DocumentDuplicate;
import com.matterial.mtr.api.object.DocumentDuplicates;
import com.matterial.mtr.api.object.DocumentForPdfConversion;
import com.matterial.mtr.api.object.DocumentLock;
import com.matterial.mtr.api.object.DocumentNotification;
import com.matterial.mtr.api.object.ExtensionValue;
import com.matterial.mtr.api.object.IndexNotification;
import com.matterial.mtr.api.object.Invitee;
import com.matterial.mtr.api.object.Language;
import com.matterial.mtr.api.object.Licence;
import com.matterial.mtr.api.object.LicenceUsage;
import com.matterial.mtr.api.object.ListResult;
import com.matterial.mtr.api.object.ListResultEntry;
import com.matterial.mtr.api.object.LoginData;
import com.matterial.mtr.api.object.LoginDataNotification;
import com.matterial.mtr.api.object.Logon;
import com.matterial.mtr.api.object.MtrMessage;
import com.matterial.mtr.api.object.MtrVersion;
import com.matterial.mtr.api.object.Notification;
import com.matterial.mtr.api.object.NotificationContainer;
import com.matterial.mtr.api.object.PasswordContainer;
import com.matterial.mtr.api.object.Permissions;
import com.matterial.mtr.api.object.Person;
import com.matterial.mtr.api.object.PersonClientContainer;
import com.matterial.mtr.api.object.PersonClientContainerList;
import com.matterial.mtr.api.object.PersonContainer;
import com.matterial.mtr.api.object.PreferenceMapContainer;
import com.matterial.mtr.api.object.PreferenceNotification;
import com.matterial.mtr.api.object.Role;
import com.matterial.mtr.api.object.RoleRight;
import com.matterial.mtr.api.object.SavedSearch;
import com.matterial.mtr.api.object.SavedSearchParameter;
import com.matterial.mtr.api.object.SearchAutocompleteSuggest;
import com.matterial.mtr.api.object.SearchResult;
import com.matterial.mtr.api.object.SearchResultEntry;
import com.matterial.mtr.api.object.Signup;
import com.matterial.mtr.api.object.SimpleValue;
import com.matterial.mtr.api.object.Task;
import com.matterial.mtr.api.object.TaskStatus;
import com.matterial.mtr.api.object.TempFileDescriptor;
import com.matterial.mtr.api.object.TrackingItem;
import com.matterial.mtr.api.object.WildflyDataSource;
import com.matterial.mtr.api.object.teams.TeamsEvent;
import com.matterial.mtr.api.object.teams.TeamsEvent.EventCategory;
import com.matterial.mtr.api.object.teams.TeamsEvents;

import lombok.extern.slf4j.Slf4j;

/**
 * <strong>SampleCreatorTest</strong>
 */
@Slf4j
public class SampleCreatorTest {

    private static final long SAMPLE_DATE_IN_SECONDS = LocalDateTime.of(2017, 3, 1, 14, 25).toEpochSecond(ZoneOffset.UTC);
    private static final OffsetDateTime SAMPLE_OFFSET_DATE_TIME = LocalDateTime.of(2017, 3, 1, 14, 25).atOffset(ZoneOffset.UTC);
    private static final LocalDate SAMPLE_LOCAL_DATE = LocalDate.of(2017, 3, 1);
    private static final long SAMPLE_LONG = 34L;
    private static final int SAMPLE_INT = 65;
    private static final boolean SAMPLE_BOOLEAN = true;
    private static final EventCategory SAMPLE_EVENT_CATEGORY = EventCategory.Notification;

    private static final ObjectMapper MAPPER = JsonMapper.builder()
                    .addModule(new JavaTimeModule())
                    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                    .configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS, false)
                    .configure(SerializationFeature.INDENT_OUTPUT, true)
                    .build();

    @BeforeClass
    public static void init() {
        // *** currently nothing;
    }

    @Test
    public void createSchema() {
        log.info("createSchema() - GENERATING JSON SCHEMA");
        File outputDirectory = new File(System.getProperty("user.dir")+"/src/resources/json-schema/");
        log.info("createSchema() - outputDirectory: {}", outputDirectory.getAbsolutePath());
        if(!outputDirectory.exists()) {
            outputDirectory.mkdirs();
        }
        assertTrue(outputDirectory.exists());
        SchemaGeneratorConfigBuilder configBuilder = new SchemaGeneratorConfigBuilder(SchemaVersion.DRAFT_2019_09,
                                                                                      new OptionPreset(Option.SCHEMA_VERSION_INDICATOR,
                                                                                                       Option.NONPUBLIC_NONSTATIC_FIELDS_WITH_GETTERS,
                                                                                                       Option.DEFINITIONS_FOR_ALL_OBJECTS,
                                                                                                       Option.NULLABLE_FIELDS_BY_DEFAULT));
        SchemaGeneratorConfig config = configBuilder.build();
        SchemaGenerator generator = new SchemaGenerator(config);

        // *** for each object in package;
        printJsonSchema(generator, outputDirectory, AccountSetting.class);
        printJsonSchema(generator, outputDirectory, Activity.class);
        printJsonSchema(generator, outputDirectory, AdditionalProperty.class);
        printJsonSchema(generator, outputDirectory, Address.class);
        printJsonSchema(generator, outputDirectory, Attachment.class);
        printJsonSchema(generator, outputDirectory, BatchActionAdditionalData.class);
        printJsonSchema(generator, outputDirectory, BatchActionError.class);
        printJsonSchema(generator, outputDirectory, BatchActionStatusNotification.class);
        printJsonSchema(generator, outputDirectory, Category.class);
        printJsonSchema(generator, outputDirectory, CategoryNotification.class);
        printJsonSchema(generator, outputDirectory, CategoryType.class);
        printJsonSchema(generator, outputDirectory, Client.class);
        printJsonSchema(generator, outputDirectory, ClipboardNotification.class);
        printJsonSchema(generator, outputDirectory, Comment.class);
        printJsonSchema(generator, outputDirectory, CommunicationData.class);
        printJsonSchema(generator, outputDirectory, CompanyInfoItem.class);
        printJsonSchema(generator, outputDirectory, ContactImage.class);
        printJsonSchema(generator, outputDirectory, Credential.class);
        printJsonSchema(generator, outputDirectory, CredentialWithInvitationText.class);
        printJsonSchema(generator, outputDirectory, DataSource.class);
        printJsonSchema(generator, outputDirectory, Document.class);
        printJsonSchema(generator, outputDirectory, DocumentChangeLog.class);
        printJsonSchema(generator, outputDirectory, DocumentChangeLogMap.class);
        printJsonSchema(generator, outputDirectory, DocumentClipBoardEntry.class);
        printJsonSchema(generator, outputDirectory, DocumentDuplicate.class);
        printJsonSchema(generator, outputDirectory, DocumentDuplicates.class);
        printJsonSchema(generator, outputDirectory, DocumentForPdfConversion.class);
        printJsonSchema(generator, outputDirectory, DocumentLock.class);
        printJsonSchema(generator, outputDirectory, DocumentNotification.class);
        printJsonSchema(generator, outputDirectory, ExtensionValue.class);
        printJsonSchema(generator, outputDirectory, IndexNotification.class);
        printJsonSchema(generator, outputDirectory, Invitee.class);
        printJsonSchema(generator, outputDirectory, Language.class);
        printJsonSchema(generator, outputDirectory, Licence.class);
        printJsonSchema(generator, outputDirectory, LicenceUsage.class);
        printJsonSchema(generator, outputDirectory, ListResult.class);
        printJsonSchema(generator, outputDirectory, ListResultEntry.class);
        printJsonSchema(generator, outputDirectory, LoginData.class);
        printJsonSchema(generator, outputDirectory, LoginDataNotification.class);
        printJsonSchema(generator, outputDirectory, Logon.class);
        printJsonSchema(generator, outputDirectory, MtrMessage.class);
        printJsonSchema(generator, outputDirectory, MtrVersion.class);
        printJsonSchema(generator, outputDirectory, Notification.class);
        printJsonSchema(generator, outputDirectory, NotificationContainer.class);
        printJsonSchema(generator, outputDirectory, PasswordContainer.class);
        printJsonSchema(generator, outputDirectory, Permissions.class);
        printJsonSchema(generator, outputDirectory, Person.class);
        printJsonSchema(generator, outputDirectory, PersonClientContainer.class);
        printJsonSchema(generator, outputDirectory, PersonClientContainerList.class);
        printJsonSchema(generator, outputDirectory, PersonContainer.class);
        printJsonSchema(generator, outputDirectory, PreferenceMapContainer.class);
        printJsonSchema(generator, outputDirectory, PreferenceNotification.class);
        printJsonSchema(generator, outputDirectory, Role.class);
        printJsonSchema(generator, outputDirectory, RoleRight.class);
        printJsonSchema(generator, outputDirectory, SavedSearch.class);
        printJsonSchema(generator, outputDirectory, SavedSearchParameter.class);
        printJsonSchema(generator, outputDirectory, SearchAutocompleteSuggest.class);
        printJsonSchema(generator, outputDirectory, SearchResult.class);
        printJsonSchema(generator, outputDirectory, SearchResultEntry.class);
        printJsonSchema(generator, outputDirectory, Signup.class);
        printJsonSchema(generator, outputDirectory, SimpleValue.class);
        printJsonSchema(generator, outputDirectory, Task.class);
        printJsonSchema(generator, outputDirectory, TaskStatus.class);
        printJsonSchema(generator, outputDirectory, TempFileDescriptor.class);
        printJsonSchema(generator, outputDirectory, TrackingItem.class);
        printJsonSchema(generator, outputDirectory, WildflyDataSource.class);
        // *** special TeamsEvents;
        printJsonSchema(generator, outputDirectory, TeamsEvents.class);
        printJsonSchema(generator, outputDirectory, TeamsEvent.class);
    }

    private void printJsonSchema(SchemaGenerator generator, File outputDirectory, Class<?> clazz) {
        try {
            log.info("printJsonSchema() - GENERATING SCHEMA FOR CLASS {}", clazz.getSimpleName());
            JsonNode jsonSchema = generator.generateSchema(clazz);
            String str = MAPPER.writeValueAsString(jsonSchema);
            File f = new File(outputDirectory, clazz.getSimpleName()+".json");
            FileWriter fw = new FileWriter(f);
            IOUtils.write(str, fw);
            fw.close();
            assertTrue(f.exists());
        }
        catch(Exception e) {
            log.error("printJsonSchema() - ", e);
        }
    }

    @Test
    public void createJson() {
        log.info("createJson() - GENERATING JSON");
        File outputDirectory = new File(System.getProperty("user.dir")+"/src/resources/json-sample/");
        log.info("createJson() - outputDirectory: {}", outputDirectory.getAbsolutePath());
        if(!outputDirectory.exists()) {
            outputDirectory.mkdirs();
        }
        assertTrue(outputDirectory.exists());

        // *** for each object in package;
        printJson(outputDirectory, AccountSetting.class);
        printJson(outputDirectory, Activity.class);
        printJson(outputDirectory, AdditionalProperty.class);
        printJson(outputDirectory, Address.class);
        printJson(outputDirectory, Attachment.class);
        printJson(outputDirectory, BatchActionAdditionalData.class);
        printJson(outputDirectory, BatchActionError.class);
        printJson(outputDirectory, BatchActionStatusNotification.class);
        printJson(outputDirectory, Category.class);
        printJson(outputDirectory, CategoryNotification.class);
        printJson(outputDirectory, CategoryType.class);
        printJson(outputDirectory, Client.class);
        printJson(outputDirectory, ClipboardNotification.class);
        printJson(outputDirectory, Comment.class);
        printJson(outputDirectory, CommunicationData.class);
        printJson(outputDirectory, CompanyInfoItem.class);
        printJson(outputDirectory, ContactImage.class);
        printJson(outputDirectory, Credential.class);
        printJson(outputDirectory, CredentialWithInvitationText.class);
        printJson(outputDirectory, DataSource.class);
        printJson(outputDirectory, Document.class);
        printJson(outputDirectory, DocumentChangeLog.class);
        printJson(outputDirectory, DocumentChangeLogMap.class);
        printJson(outputDirectory, DocumentClipBoardEntry.class);
        printJson(outputDirectory, DocumentDuplicate.class);
        printJson(outputDirectory, DocumentDuplicates.class);
        printJson(outputDirectory, DocumentForPdfConversion.class);
        printJson(outputDirectory, DocumentLock.class);
        printJson(outputDirectory, DocumentNotification.class);
        printJson(outputDirectory, ExtensionValue.class);
        printJson(outputDirectory, IndexNotification.class);
        printJson(outputDirectory, Invitee.class);
        printJson(outputDirectory, Language.class);
        printJson(outputDirectory, Licence.class);
        printJson(outputDirectory, LicenceUsage.class);
        printJson(outputDirectory, ListResult.class);
        printJson(outputDirectory, ListResultEntry.class);
        printJson(outputDirectory, LoginData.class);
        printJson(outputDirectory, LoginDataNotification.class);
        printJson(outputDirectory, Logon.class);
        printJson(outputDirectory, MtrMessage.class);
        printJson(outputDirectory, MtrVersion.class);
        printJson(outputDirectory, Notification.class);
        printJson(outputDirectory, NotificationContainer.class);
        printJson(outputDirectory, PasswordContainer.class);
        printJson(outputDirectory, Permissions.class);
        printJson(outputDirectory, Person.class);
        printJson(outputDirectory, PersonClientContainer.class);
        printJson(outputDirectory, PersonClientContainerList.class);
        printJson(outputDirectory, PersonContainer.class);
        printJson(outputDirectory, PreferenceMapContainer.class);
        printJson(outputDirectory, PreferenceNotification.class);
        printJson(outputDirectory, Role.class);
        printJson(outputDirectory, RoleRight.class);
        printJson(outputDirectory, SavedSearch.class);
        printJson(outputDirectory, SavedSearchParameter.class);
        printJson(outputDirectory, SearchAutocompleteSuggest.class);
        printJson(outputDirectory, SearchResult.class);
        printJson(outputDirectory, SearchResultEntry.class);
        printJson(outputDirectory, Signup.class);
        printJson(outputDirectory, SimpleValue.class);
        printJson(outputDirectory, Task.class);
        printJson(outputDirectory, TaskStatus.class);
        printJson(outputDirectory, TempFileDescriptor.class);
        printJson(outputDirectory, TrackingItem.class);
        printJson(outputDirectory, WildflyDataSource.class);
        // *** special TeamsEvents;
        printJson(outputDirectory, TeamsEvents.class);
        printJson(outputDirectory, TeamsEvent.class);
    }

    private void printJson(File outputDirectory, Class<?> clazz) {
        File f = null;
        String json = null;
        try {
            log.info("printJson() - GENERATING JSON FOR CLASS {}", clazz.getSimpleName());
            Object obj = createApiObject(clazz);
            assertTrue(obj != null);
            json = MAPPER.writeValueAsString(obj);
            f = new File(outputDirectory, clazz.getSimpleName()+".json");
            FileWriter fw = new FileWriter(f);
            IOUtils.write(json, fw);
            fw.close();
        }
        catch(Exception e) {
            log.error("printJson() - ", e);
            assertTrue(false);
        }
        assertTrue(f != null);
        assertTrue(f.exists());
        assertTrue(json != null);
        assertTrue(!json.trim().isEmpty());
        this.checkJsonToObject(json, clazz);
   }

    private void checkJsonToObject(String json, Class<?> clazz) {
        Object obj = null;
        try {
            // *** json to object;
            obj = MAPPER.readerFor(clazz)
                        .readValue(json);
            assertTrue(obj != null);
            this.checkApiObject(obj, clazz);
        }
        catch(Exception e) {
            log.error("checkJsonToObject() - ", e);
            assertTrue(false);
        }
    }

    @Test
    public void createXml() {
        log.info("createXml() - GENERATING XML");
        File outputDirectory = new File(System.getProperty("user.dir")+"/src/resources/xml-sample/");
        log.info("createXml() - outputDirectory: {}", outputDirectory.getAbsolutePath());
        if(!outputDirectory.exists()) {
            outputDirectory.mkdirs();
        }
        assertTrue(outputDirectory.exists());

        // *** for each object in package;
        printXml(outputDirectory, AccountSetting.class);
        printXml(outputDirectory, Activity.class);
        printXml(outputDirectory, AdditionalProperty.class);
        printXml(outputDirectory, Address.class);
        printXml(outputDirectory, Attachment.class);
        printXml(outputDirectory, BatchActionAdditionalData.class);
        printXml(outputDirectory, BatchActionError.class);
        printXml(outputDirectory, BatchActionStatusNotification.class);
        printXml(outputDirectory, Category.class);
        printXml(outputDirectory, CategoryNotification.class);
        printXml(outputDirectory, CategoryType.class);
        printXml(outputDirectory, Client.class);
        printXml(outputDirectory, ClipboardNotification.class);
        printXml(outputDirectory, Comment.class);
        printXml(outputDirectory, CommunicationData.class);
        printXml(outputDirectory, CompanyInfoItem.class);
        printXml(outputDirectory, ContactImage.class);
        printXml(outputDirectory, Credential.class);
        printXml(outputDirectory, CredentialWithInvitationText.class);
        printXml(outputDirectory, DataSource.class);
        printXml(outputDirectory, Document.class);
        printXml(outputDirectory, DocumentChangeLog.class);
        printXml(outputDirectory, DocumentChangeLogMap.class);
        printXml(outputDirectory, DocumentClipBoardEntry.class);
        printXml(outputDirectory, DocumentDuplicate.class);
        printXml(outputDirectory, DocumentDuplicates.class);
        printXml(outputDirectory, DocumentForPdfConversion.class);
        printXml(outputDirectory, DocumentLock.class);
        printXml(outputDirectory, DocumentNotification.class);
        printXml(outputDirectory, ExtensionValue.class);
        printXml(outputDirectory, IndexNotification.class);
        printXml(outputDirectory, Invitee.class);
        printXml(outputDirectory, Language.class);
        printXml(outputDirectory, Licence.class);
        printXml(outputDirectory, LicenceUsage.class);
        printXml(outputDirectory, ListResult.class);
        printXml(outputDirectory, ListResultEntry.class);
        printXml(outputDirectory, LoginData.class);
        printXml(outputDirectory, LoginDataNotification.class);
        printXml(outputDirectory, Logon.class);
        printXml(outputDirectory, MtrMessage.class);
        printXml(outputDirectory, MtrVersion.class);
        printXml(outputDirectory, Notification.class);
        printXml(outputDirectory, NotificationContainer.class);
        printXml(outputDirectory, PasswordContainer.class);
        printXml(outputDirectory, Permissions.class);
        printXml(outputDirectory, Person.class);
        printXml(outputDirectory, PersonClientContainer.class);
        printXml(outputDirectory, PersonClientContainerList.class);
        printXml(outputDirectory, PersonContainer.class);
        printXml(outputDirectory, PreferenceMapContainer.class);
        printXml(outputDirectory, PreferenceNotification.class);
        printXml(outputDirectory, Role.class);
        printXml(outputDirectory, RoleRight.class);
        printXml(outputDirectory, SavedSearch.class);
        printXml(outputDirectory, SavedSearchParameter.class);
        printXml(outputDirectory, SearchAutocompleteSuggest.class);
        printXml(outputDirectory, SearchResult.class);
        printXml(outputDirectory, SearchResultEntry.class);
        printXml(outputDirectory, Signup.class);
        printXml(outputDirectory, SimpleValue.class);
        printXml(outputDirectory, Task.class);
        printXml(outputDirectory, TaskStatus.class);
        printXml(outputDirectory, TempFileDescriptor.class);
        printXml(outputDirectory, TrackingItem.class);
        printXml(outputDirectory, WildflyDataSource.class);
    }

    private void printXml(File outputDirectory, Class<?> clazz) {
        File f = null;
        String xml = null;
        try {
            log.info("printXml() - GENERATING XML FOR CLASS {}", clazz.getSimpleName());
            Object obj = createApiObject(clazz);
            assertTrue(obj != null);
            StringWriter sw = new StringWriter();
            JAXB.marshal(obj, sw);
            xml = sw.toString();
            f = new File(outputDirectory, clazz.getSimpleName()+".xml");
            FileWriter fw = new FileWriter(f);
            IOUtils.write(xml, fw);
            fw.close();
            assertTrue(f.exists());
        }
        catch(Exception e) {
            log.error("printXml() - ", e);
        }
        assertTrue(f != null);
        assertTrue(f.exists());
        assertTrue(xml != null);
        assertTrue(!xml.trim().isEmpty());
        this.checkXmlToObject(xml, clazz);
    }

    private void checkXmlToObject(String xml, Class<?> clazz) {
        Object obj = null;
        try {
            // *** xml to object;
            obj = JAXB.unmarshal(new StringReader(xml), clazz);
            assertTrue(obj != null);
            this.checkApiObject(obj, clazz);
        }
        catch(Exception e) {
            log.error("checkXmlToObject() - ", e);
            assertTrue(false);
        }
    }

    private Object createApiObject(Class<?> clazz) throws Exception {
        Object obj = clazz.getDeclaredConstructor().newInstance();
        for (Method m : clazz.getMethods()) {
            if(m != null && m.getName().startsWith("set") && m.getParameterCount() == 1) {
                Class<?> paramType = m.getParameterTypes()[0];
                if(paramType.getName().equals(Object.class.getName())) {
                    m.invoke(obj, m.getName().substring(3));
                }
                else if(paramType.getName().equals(String.class.getName())) {
                    m.invoke(obj, m.getName().substring(3));
                }
                else if(paramType.getName().equals(Long.class.getName()) || paramType.getName().equals("long")) {
                    if(m.getName().endsWith("InSeconds")) {
                        m.invoke(obj, SAMPLE_DATE_IN_SECONDS);
                    }
                    else {
                        m.invoke(obj, SAMPLE_LONG);
                    }
                }
                else if(paramType.getName().equals(Integer.class.getName()) || paramType.getName().equals("int")) {
                    m.invoke(obj, SAMPLE_INT);
                }
                else if(paramType.getName().equals(Boolean.class.getName()) || paramType.getName().equals("boolean")) {
                    m.invoke(obj, SAMPLE_BOOLEAN);
                }
                else if(paramType.getName().equals(OffsetDateTime.class.getName())) {
                    m.invoke(obj, SAMPLE_OFFSET_DATE_TIME);
                }
                else if(paramType.getName().equals(LocalDate.class.getName())) {
                    m.invoke(obj, SAMPLE_LOCAL_DATE);
                }
                else if(paramType.getName().equals(EventCategory.class.getName())) {
                    m.invoke(obj, SAMPLE_EVENT_CATEGORY);
                }
            }
        }
        return obj;
    }

    private Object checkApiObject(Object obj, Class<?> clazz) throws Exception {
        for (Method m : clazz.getMethods()) {
            if(m != null && m.getName().startsWith("get") && m.getParameterCount() == 0) {
                Class<?> returnType = m.getReturnType();
                if(m.getName().equals("getJsonType")) {
                    Object result = m.invoke(obj);
                    assertTrue(result != null);
                    assertTrue(result instanceof String);
                    assertTrue(((String)result).equals(clazz.getSimpleName()));
                }
                else if(m.getName().equals("getMessageSubject")) {
                    // *** do not check;
                }
                else if(m.getName().equals("getMessageBody")) {
                    // *** do not check;
                }
                else if(m.getName().equals("getDocumentUrl")) {
                    // *** do not check;
                }
                else if(returnType.getName().equals(Object.class.getName())) {
                    Object result = m.invoke(obj);
                    assertTrue(result != null);
                    assertTrue(result instanceof String);
                    assertTrue(((String)result).equals(m.getName().substring(3)));
                }
                else if(returnType.getName().equals(String.class.getName())) {
                    Object result = m.invoke(obj);
                    assertTrue(result != null);
                    assertTrue(result instanceof String);
                    assertTrue(((String)result).equals(m.getName().substring(3)));
                }
                else if(returnType.getName().equals(Long.class.getName()) || returnType.getName().equals("long")) {
                    Object result = m.invoke(obj);
                    assertTrue(result != null);
                    assertTrue(result instanceof Long);
                    assertTrue(((Long)result) > 0L);
                    if(m.getName().endsWith("InSeconds")) {
                        assertTrue(((Long)result) == SAMPLE_DATE_IN_SECONDS);
                    }
                    else {
                        assertTrue(((Long)result) == SAMPLE_LONG);
                    }
                }
                else if(returnType.getName().equals(Integer.class.getName()) || returnType.getName().equals("int")) {
                    Object result = m.invoke(obj);
                    assertTrue(result != null);
                    assertTrue(result instanceof Integer);
                    assertTrue(((Integer)result) > 0L);
                    assertTrue(((Integer)result) == SAMPLE_INT);
                }
                else if(returnType.getName().equals(Boolean.class.getName()) || returnType.getName().equals("boolean")) {
                    Object result = m.invoke(obj);
                    assertTrue(result != null);
                    assertTrue(result instanceof Boolean);
                    assertTrue(((Boolean)result) == SAMPLE_BOOLEAN);
                }
                else if(returnType.getName().equals(OffsetDateTime.class.getName())) {
                    Object result = m.invoke(obj);
                    assertTrue(result != null);
                    assertTrue(result instanceof OffsetDateTime);
                    assertTrue(((OffsetDateTime)result).equals(SAMPLE_OFFSET_DATE_TIME));
                }
                else if(returnType.getName().equals(LocalDate.class.getName())) {
                    Object result = m.invoke(obj);
                    assertTrue(result != null);
                    assertTrue(result instanceof LocalDate);
                    assertTrue(((LocalDate)result).equals(SAMPLE_LOCAL_DATE));
                }
                else if(returnType.getName().equals(EventCategory.class.getName())) {
                    Object result = m.invoke(obj);
                    assertTrue(result != null);
                    assertTrue(result instanceof EventCategory);
                    assertTrue(((EventCategory)result) == SAMPLE_EVENT_CATEGORY);
                }
            }
        }
        return obj;
    }

}
