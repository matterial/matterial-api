package com.matterial.mtr.api;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.matterial.mtr.api.object.AccountSetting;
import com.matterial.mtr.api.object.Activity;
import com.matterial.mtr.api.object.AdditionalProperty;
import com.matterial.mtr.api.object.Address;
import com.matterial.mtr.api.object.Attachment;
import com.matterial.mtr.api.object.BatchActionAdditionalData;
import com.matterial.mtr.api.object.BatchActionError;
import com.matterial.mtr.api.object.BatchActionStatusNotification;
import com.matterial.mtr.api.object.Category;
import com.matterial.mtr.api.object.CategoryNotification;
import com.matterial.mtr.api.object.CategoryType;
import com.matterial.mtr.api.object.Client;
import com.matterial.mtr.api.object.ClipboardNotification;
import com.matterial.mtr.api.object.Comment;
import com.matterial.mtr.api.object.CommunicationData;
import com.matterial.mtr.api.object.ContactImage;
import com.matterial.mtr.api.object.Credential;
import com.matterial.mtr.api.object.CredentialWithInvitationText;
import com.matterial.mtr.api.object.DataSource;
import com.matterial.mtr.api.object.Document;
import com.matterial.mtr.api.object.DocumentChangeLog;
import com.matterial.mtr.api.object.DocumentChangeLogMap;
import com.matterial.mtr.api.object.DocumentClipBoardEntry;
import com.matterial.mtr.api.object.DocumentDuplicate;
import com.matterial.mtr.api.object.DocumentDuplicates;
import com.matterial.mtr.api.object.DocumentForPdfConversion;
import com.matterial.mtr.api.object.DocumentLock;
import com.matterial.mtr.api.object.DocumentNotification;
import com.matterial.mtr.api.object.ExtensionValue;
import com.matterial.mtr.api.object.IndexNotification;
import com.matterial.mtr.api.object.Invitee;
import com.matterial.mtr.api.object.Language;
import com.matterial.mtr.api.object.Licence;
import com.matterial.mtr.api.object.LicenceUsage;
import com.matterial.mtr.api.object.ListResult;
import com.matterial.mtr.api.object.ListResultEntry;
import com.matterial.mtr.api.object.LoginData;
import com.matterial.mtr.api.object.LoginDataNotification;
import com.matterial.mtr.api.object.Logon;
import com.matterial.mtr.api.object.MtrMessage;
import com.matterial.mtr.api.object.Notification;
import com.matterial.mtr.api.object.NotificationContainer;
import com.matterial.mtr.api.object.PasswordContainer;
import com.matterial.mtr.api.object.Permissions;
import com.matterial.mtr.api.object.Person;
import com.matterial.mtr.api.object.PersonClientContainer;
import com.matterial.mtr.api.object.PersonClientContainerList;
import com.matterial.mtr.api.object.PersonContainer;
import com.matterial.mtr.api.object.PreferenceMapContainer;
import com.matterial.mtr.api.object.PreferenceNotification;
import com.matterial.mtr.api.object.Role;
import com.matterial.mtr.api.object.RoleRight;
import com.matterial.mtr.api.object.SavedSearch;
import com.matterial.mtr.api.object.SavedSearchParameter;
import com.matterial.mtr.api.object.SearchAutocompleteSuggest;
import com.matterial.mtr.api.object.SearchResult;
import com.matterial.mtr.api.object.SearchResultEntry;
import com.matterial.mtr.api.object.Signup;
import com.matterial.mtr.api.object.SimpleValue;
import com.matterial.mtr.api.object.Task;
import com.matterial.mtr.api.object.TaskStatus;
import com.matterial.mtr.api.object.TempFileDescriptor;
import com.matterial.mtr.api.object.TrackingItem;
import com.matterial.mtr.api.object.WildflyDataSource;

import de.a9d3.testing.checks.GetterIsSetterCheck;
import de.a9d3.testing.executer.SingleThreadExecutor;

/**
 * <strong>BasicTest</strong>
 */
public class BasicTest {

    private static final String EXCLUDE_PATTERN = "([gs]etJsonType){0,1}" +
                                                  "([\\w]{2,3}PartialUpdate){0,1}" +
                                                  "([\\w]{2,3}Active){0,1}" +
                                                  "([\\w]{2,3}Keys){0,1}" +
                                                  "([\\w]{2,3}Indexable){0,1}" +
                                                  "([\\w]{2,3}Source){0,1}";

    private static final Logger logger = LoggerFactory.getLogger(BasicTest.class);

    @Test
    public void apiObjectTest() {
        logger.info("apiObjectTest() - TESTING API OBJECTS");
        SingleThreadExecutor executor = new SingleThreadExecutor();

        checkApiObject(executor, AccountSetting.class);
        checkApiObject(executor, Activity.class);
        checkApiObject(executor, AdditionalProperty.class);
        checkApiObject(executor, Address.class);
        checkApiObject(executor, Attachment.class);
        checkApiObject(executor, BatchActionAdditionalData.class);
        checkApiObject(executor, BatchActionError.class);
        checkApiObject(executor, BatchActionStatusNotification.class);
        checkApiObject(executor, Category.class);
        checkApiObject(executor, CategoryNotification.class);
        checkApiObject(executor, CategoryType.class);
        checkApiObject(executor, Client.class);
        checkApiObject(executor, ClipboardNotification.class);
        checkApiObject(executor, Comment.class);
        checkApiObject(executor, CommunicationData.class);
        checkApiObject(executor, ContactImage.class);
        checkApiObject(executor, Credential.class);
        checkApiObject(executor, CredentialWithInvitationText.class);
        checkApiObject(executor, DataSource.class);
        checkApiObject(executor, Document.class);
        checkApiObject(executor, DocumentChangeLog.class);
        checkApiObject(executor, DocumentChangeLogMap.class);
        checkApiObject(executor, DocumentClipBoardEntry.class);
        checkApiObject(executor, DocumentDuplicate.class);
        checkApiObject(executor, DocumentDuplicates.class);
        checkApiObject(executor, DocumentForPdfConversion.class);
        checkApiObject(executor, DocumentLock.class);
        checkApiObject(executor, DocumentNotification.class);
        checkApiObject(executor, ExtensionValue.class);
        checkApiObject(executor, IndexNotification.class);
        checkApiObject(executor, Invitee.class);
        checkApiObject(executor, Language.class);
        checkApiObject(executor, Licence.class);
        checkApiObject(executor, LicenceUsage.class);
        checkApiObject(executor, ListResult.class);
        checkApiObject(executor, ListResultEntry.class);
        checkApiObject(executor, LoginData.class);
        checkApiObject(executor, LoginDataNotification.class);
        checkApiObject(executor, Logon.class);
        checkApiObject(executor, MtrMessage.class);
        checkApiObject(executor, Notification.class);
        checkApiObject(executor, NotificationContainer.class);
        checkApiObject(executor, PasswordContainer.class);
        checkApiObject(executor, Permissions.class);
        checkApiObject(executor, Person.class);
        checkApiObject(executor, PersonClientContainer.class);
        checkApiObject(executor, PersonClientContainerList.class);
        checkApiObject(executor, PersonContainer.class);
        checkApiObject(executor, PreferenceMapContainer.class);
        checkApiObject(executor, PreferenceNotification.class);
        checkApiObject(executor, Role.class);
        checkApiObject(executor, RoleRight.class);
        checkApiObject(executor, SavedSearch.class);
        checkApiObject(executor, SavedSearchParameter.class);
        checkApiObject(executor, SearchAutocompleteSuggest.class);
        checkApiObject(executor, SearchResult.class);
        checkApiObject(executor, SearchResultEntry.class);
        checkApiObject(executor, Signup.class);
        checkApiObject(executor, SimpleValue.class);
        checkApiObject(executor, Task.class);
        checkApiObject(executor, TaskStatus.class);
        checkApiObject(executor, TempFileDescriptor.class);
        checkApiObject(executor, TrackingItem.class);
        checkApiObject(executor, WildflyDataSource.class);
    }

    private void checkApiObject(SingleThreadExecutor executor, Class<?> clazz) {
        logger.info("checkApiObject() - TESTING API OBJECT {}", clazz.getSimpleName());
        assertTrue(executor.execute(clazz,
                                    Arrays.asList(new GetterIsSetterCheck(EXCLUDE_PATTERN))));
    }

}
